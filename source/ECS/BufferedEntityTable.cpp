#include "ECS/BufferedEntityTable.hpp"

#include <utility>

namespace Suzu {
    BufferedEntityTable::BufferedEntityTable() noexcept : EntityTable() {
        dataBuffer = (void**) std::malloc(sizeof(void*));
    }

    BufferedEntityTable::~BufferedEntityTable() noexcept {
        clean();
    }

    BufferedEntityTable::BufferedEntityTable(BufferedEntityTable&& other) noexcept {
        // Assign nullptr to prevent freeing random memory
        dataBuffer = nullptr;

        *this = std::move(other);
    }

    BufferedEntityTable& BufferedEntityTable::operator=(BufferedEntityTable&& other) noexcept {
        if (this == &other) return *this;

        clean();

        dataBuffer = other.dataBuffer;
        other.dataBuffer = nullptr;

        EntityTable::operator=(std::move(other));

        return *this;
    }

    void BufferedEntityTable::swap() noexcept {
        std::swap(data, dataBuffer);
    }

    void BufferedEntityTable::clean() noexcept {
        for (int i = 0; i < nextComp; ++i) {
            if (dataBuffer) std::free(dataBuffer[i]);
        }
        std::free(dataBuffer);
    }

    void BufferedEntityTable::createEmptyCompTable(unsigned int id, std::size_t size) noexcept {
        EntityTable::createEmptyCompTable(id, size);
        dataBuffer[id] = (void*) std::malloc(size * maxEnts);
    }

    void BufferedEntityTable::doubleMaxEnts() noexcept {
        EntityTable::doubleMaxEnts();
        for (int i = 0; i < nextComp; ++i) {
            dataBuffer[i] = (void*) std::realloc(dataBuffer[i], compMetadata[i].size * maxEnts);
        }
    }

    void BufferedEntityTable::doubleMaxComps() noexcept {
        EntityTable::doubleMaxComps();
        dataBuffer = (void**) std::realloc(dataBuffer, sizeof(void*) * maxComps);
    }
}
