#include "ECS/EntityTable.hpp"

#include <cstdlib>
#include <stdexcept>
#include <utility>

#include "ECS/Entity.hpp"

namespace Suzu {
    EntityTable::EntityTable() noexcept {
        maxEnts  = 1;
        maxComps = 1;

        nextEnt  = 0;
        nextComp = 0;

        data = (void**) std::malloc(sizeof(void*));

        entMetadata   = (EntMeta*)    std::malloc(sizeof(EntMeta));
        compMetadata  = (CompMeta*)   std::malloc(sizeof(CompMeta));
        valueMetadata = (ValueMeta**) std::malloc(sizeof(ValueMeta*));
    }

    EntityTable::~EntityTable() noexcept {
        clean();
    }

    EntityTable::EntityTable(EntityTable&& other) noexcept // Move
    {
        // Assign nullptr to prevent freeing random memory
        data          = nullptr;
        entMetadata   = nullptr;
        compMetadata  = nullptr;
        valueMetadata = nullptr;
        nextComp      = 0;

        *this = std::move(other);
    }

    EntityTable& EntityTable::operator=(EntityTable&& other) noexcept { // Move assignment
        if (this == &other) return *this;

        clean();

        entLocalId  = std::move(other.entLocalId);
        compLocalId = std::move(other.compLocalId);

        maxEnts  = other.maxEnts;
        maxComps = other.maxComps;
        nextEnt  = other.nextEnt;
        nextComp = other.nextComp;

        data          = other.data;
        entMetadata   = other.entMetadata;
        compMetadata  = other.compMetadata;
        valueMetadata = other.valueMetadata;

        other.data          = nullptr;
        other.entMetadata   = nullptr;
        other.compMetadata  = nullptr;
        other.valueMetadata = nullptr;

        return *this;
    }

    template <>
    void EntityTable::use(Component<void> comp) noexcept {
        if (compLocalId.count(comp.id) != 0) return;
        compLocalId[comp.id] = nextComp;

        if (nextComp >= maxComps) doubleMaxComps();
        createEmptyCompTable(nextComp, 0);

        valueMetadata[nextComp] = (ValueMeta*) std::malloc(sizeof(ValueMeta) * maxEnts);
        std::memset(valueMetadata[nextComp], 0, sizeof(ValueMeta) * maxEnts);
        compMetadata[nextComp].size = 0;

        ++nextComp;
    }

    void EntityTable::use(Entity ent) noexcept {
        if (entLocalId.count(ent.id) != 0) return;
        entLocalId[ent.id] = nextEnt;

        if (nextEnt >= maxEnts) doubleMaxEnts();
        entMetadata[nextEnt].orphan = true;

        ++nextEnt;
    }

    LocalEntity EntityTable::local(Entity ent) const {
        if (!entLocalId.contains(ent.id)) {
            throw std::invalid_argument("EntityTable::local(Entity): EntityTable does not use Entity!");
        }
        auto it = entLocalId.find(ent.id);
        return LocalEntity(it->second);
    }

    bool& EntityTable::owns(LocalEntity ent, LocalComponent<void> comp) noexcept {
        return valueMetadata[comp.id][ent.id].exists;
    }

    bool EntityTable::cOwns(LocalEntity ent, LocalComponent<void> comp) const noexcept {
        return valueMetadata[comp.id][ent.id].exists;
    }

    bool EntityTable::has(LocalEntity ent, LocalComponent<void> comp) const noexcept {
        bool exists = valueMetadata[comp.id][ent.id].exists;
        bool orphan = entMetadata[ent.id].orphan;
        if (orphan) return exists;
        return exists || has(entMetadata[ent.id].parent, comp);
    }

    void EntityTable::adopt(LocalEntity child, LocalEntity parent) noexcept {
        orphan(child) = false;
        entMetadata[child.id].parent = parent;
    }

    bool& EntityTable::orphan(LocalEntity ent) noexcept {
        return entMetadata[ent.id].orphan;
    }

    EntityTable::Iterator EntityTable::begin() const noexcept {
        return Iterator(LocalEntity(0), *this);
    }

    EntityTable::Iterator EntityTable::end() const noexcept {
        return Iterator(LocalEntity(nextEnt), *this);
    }

    void EntityTable::clean() noexcept {
        for (int i = 0; i < nextComp; ++i) {
            if (data)          std::free(data[i]);
            if (valueMetadata) std::free(valueMetadata[i]);
        }
        std::free(data);

        std::free(entMetadata);
        std::free(compMetadata);
        std::free(valueMetadata);
    }

    void EntityTable::createEmptyCompTable(unsigned int id, std::size_t size) noexcept {
        data[id] = (void*) std::malloc(size * maxEnts);
    }

    void EntityTable::doubleMaxEnts() noexcept {
        maxEnts *= 2;
        for (int i = 0; i < nextComp; ++i) {
            data[i] = (void*) std::realloc(data[i], compMetadata[i].size * maxEnts);
            valueMetadata[i] = (ValueMeta*) std::realloc(valueMetadata[i], sizeof(ValueMeta) * maxEnts);
        }
        entMetadata = (EntMeta*) std::realloc(entMetadata, sizeof(EntMeta) * maxEnts);
    }

    void EntityTable::doubleMaxComps() noexcept {
        maxComps *= 2;
        data = (void**) std::realloc(data, sizeof(void*) * maxComps);

        compMetadata  = (CompMeta*)   std::realloc(compMetadata,  sizeof(CompMeta)   * maxComps);
        valueMetadata = (ValueMeta**) std::realloc(valueMetadata, sizeof(ValueMeta*) * maxComps);
    }

    unsigned int EntityTable::getEntId(LocalEntity ent) noexcept {
        return ent.id;
    }

    unsigned int EntityTable::getCompId(LocalComponent<void> comp) noexcept {
        return comp.id;
    }

    EntityTable::Iterator::Iterator(LocalEntity ent, const EntityTable& table) noexcept {
        this->currEnt = ent;
        this->table = &table;
    }

    void EntityTable::Iterator::require(LocalComponent<void> comp) noexcept {
        requirements.push_back(comp);
        if (!valid()) ++(*this);
    }

    EntityTable::Iterator& EntityTable::Iterator::view(bool value) noexcept {
        useHas = value;
        return *this;
    }

    EntityTable::Iterator& EntityTable::Iterator::operator++() noexcept {
        do ++currEnt.id; while (!valid());
        return *this;
    }

    LocalEntity EntityTable::Iterator::operator*() noexcept {
        return currEnt;
    }

    bool EntityTable::Iterator::valid() noexcept {
        if (*this == table->end()) return true;

        bool valid = true;
        for (auto comp : requirements) {
            valid = (useHas) ? table->has(currEnt, comp) : table->cOwns(currEnt, comp);
            if (!valid) break;
        }

        return valid;
    }

    bool operator==(const EntityTable::Iterator& lhs, const EntityTable::Iterator& rhs) noexcept {
        return lhs.currEnt == rhs.currEnt;
    }

    bool operator!=(const EntityTable::Iterator& lhs, const EntityTable::Iterator& rhs) noexcept {
        return !(lhs == rhs);
    }
}
