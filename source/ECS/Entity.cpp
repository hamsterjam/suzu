#include "ECS/Entity.hpp"

namespace Suzu {
    unsigned int Entity::nextId = 0;

    LocalEntity::LocalEntity(unsigned int id) noexcept {
        this->id = id;
    }

    bool operator==(const LocalEntity& lhs, const LocalEntity& rhs) noexcept {
        return lhs.id == rhs.id;
    }
    bool operator!=(const LocalEntity& lhs, const LocalEntity& rhs) noexcept {
        return !(lhs == rhs);
    }

    Entity::Entity() noexcept {
        id = nextId;
        ++nextId;
    }
}
