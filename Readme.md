Suzu
====

A new game engine using lessons from my previous attempts.
This time I plan to develop this in parallel with an actual game, adding features as they become nessecary.

See the [documentation](doc/index.md) for usage (although there isn't much there).

Goals
-----

* More defined entity management
* Better integrated scripting
* No more Lua, use a better language
* Rendering with Vulkan
