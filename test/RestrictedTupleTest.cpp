#include "SuzuTest.hpp"

#include "RestrictedTuple.hpp"

#include <type_traits>
#include <tuple>

using namespace Suzu;

SUITE(RestrictedTuple) {
    TEST(TypeEquivalence) {
        EXPECT(std::is_same_v<
            RestrictedTuple<int, int, char, int, bool, int, int, float, int>,
            std::tuple<char, bool, float>
        >);
    }

    TEST(ValueEquivalence) {
        int intVal = 1;
        char charVal = 2;
        bool boolVal = true;
        float floatVal = 1.5;

        auto test = makeRestrictedTuple<int>(
            intVal,
            charVal,
            intVal,
            boolVal,
            intVal,
            intVal,
            floatVal,
            intVal
        );

        EXPECT(std::tuple_size_v<decltype(test)> == 3);
        EXPECT(std::get<0>(test) == charVal);
        EXPECT(std::get<1>(test) == boolVal);
        EXPECT(std::get<2>(test) == floatVal);
    }
}
