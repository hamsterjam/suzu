#include "SuzuTest.hpp"

#include "Thief.hpp"

#include <cstdlib>
#include <ctime>

#define REPEAT_TIMES 20

class Foo {
    private:
        int readOnly;
        int writeOnly;

    public:
        int getReadOnly() { return readOnly; }
        void setWriteOnly(int value) { writeOnly = value; }
};

class GenericFoo {
    private:
        int readOnly;
        int writeOnly;

    public:
        int getReadOnly() { return readOnly; }
        void setWriteOnly(int value) { writeOnly = value; }
};

MAKE_THIEF(GenericFoo, int, readOnly);
MAKE_THIEF(GenericFoo, int, writeOnly);

SUZU_THIEF_MAKE_THIEF(Foo, int, readOnly);
SUZU_THIEF_MAKE_THIEF(Foo, int, writeOnly);

SUITE(Thief) {
    SETUP {
        std::srand(std::time(nullptr));
    }

    TEST(GenericNames) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            GenericFoo foo;

            int value = std::rand();

            STEAL(foo, GenericFoo, readOnly) = value;
            EXPECT(foo.getReadOnly() == value);

            foo.setWriteOnly(value);
            EXPECT(STEAL(foo, GenericFoo, writeOnly) == value);
        }
    }

    TEST(SuzuNames) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            Foo foo;

            int value = std::rand();

            SUZU_THIEF_STEAL(foo, Foo, readOnly) = value;
            EXPECT(foo.getReadOnly() == value);

            foo.setWriteOnly(value);
            EXPECT(SUZU_THIEF_STEAL(foo, Foo, writeOnly) == value);
        }
    }
}
