#include "SuzuTest.hpp"
#include "Thief.hpp"

#include "ECS/Component.hpp"

#include <cstdlib>
#include <ctime>

#define REPEAT_TIMES 10

using namespace Suzu;

MAKE_THIEF(ComponentInt, Component<int>, unsigned int, id);
MAKE_THIEF(ComponentVoid, Component<void>, unsigned int, id);
MAKE_THIEF(LocalComponentInt, LocalComponent<int>, unsigned int, id);
MAKE_THIEF(LocalComponentVoid, LocalComponent<void>, unsigned int, id);

SUITE(EcsComponent) {
    SETUP {
        std::srand(std::time(nullptr));
    }

    TEST(CastToVoidComponent) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            Component<int> test;
            STEAL(test, ComponentInt, id) = std::rand();
            Component<void> testVoid = (Component<void>)test;

            EXPECT(STEAL(test, ComponentInt, id) == STEAL(testVoid, ComponentVoid, id));
        }
    }

    TEST(CastToVoidLocalComponent) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            LocalComponent<int> test;
            STEAL(test, LocalComponentInt, id) = std::rand();
            LocalComponent<void> testVoid = (LocalComponent<void>)test;

            EXPECT(STEAL(test, LocalComponentInt, id) == STEAL(testVoid, LocalComponentVoid, id));
        }
    }

    TEST(UniqueId) {
        Component<int> comps[REPEAT_TIMES];
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            for (int j = i + 1; j < REPEAT_TIMES; ++j) {
                auto& comp1 = comps[i];
                auto& comp2 = comps[j];

                EXPECT(STEAL(comp1, ComponentInt, id) != STEAL(comp2, ComponentInt, id));
            }
        }
    }
}
