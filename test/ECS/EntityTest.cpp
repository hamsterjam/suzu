#include "SuzuTest.hpp"
#include "Thief.hpp"

#include "ECS/Entity.hpp"

#include <cstdlib>
#include <ctime>

#define REPEAT_TIMES 10

using namespace Suzu;

MAKE_THIEF(Entity, unsigned int, id);

SUITE(EcsEntity) {
    SETUP {
        std::srand(std::time(nullptr));
    }

    TEST(UniqueId) {
        Entity ents[REPEAT_TIMES];
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            for (int j = i + 1; j < REPEAT_TIMES; ++j) {
                auto& ent1 = ents[i];
                auto& ent2 = ents[j];

                EXPECT(STEAL(ent1, Entity, id) != STEAL(ent2, Entity, id));
            }
        }
    }
}
