#include "SuzuTest.hpp"

#include "ECS/EntityTable.hpp"

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"

#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <utility>

#define REPEAT_TIMES 20

using namespace Suzu;

SUITE(EcsEntityTable) {
    EntityTable* ecs;

    Entity testEnts[REPEAT_TIMES];
    Component<int> testComps[REPEAT_TIMES];

    Entity unusedEnt;
    Component<int> unusedComp;

    SETUP {
        std::srand(std::time(nullptr));
        ecs = new EntityTable;

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            ecs->use(testEnts[i]);
            ecs->use(testComps[i]);
        }
    }

    TEARDOWN {
        delete ecs;
    }

    TEST(UseComponent) {
        bool exception = false;
        try {
            ecs->local(testComps[0]);
        }
        catch (std::invalid_argument& e) {
            exception = true;
        }
        EXPECT(!exception);
    }

    TEST(UseEntity) {
        bool exception = false;
        try {
            ecs->local(testEnts[0]);
        }
        catch (std::invalid_argument& e) {
            exception = true;
        }
        EXPECT(!exception);
    }

    TEST(UnusedComponent) {
        bool exception = false;
        try {
            ecs->local(unusedComp);
        }
        catch (std::invalid_argument& e) {
            exception = true;
        }
        EXPECT(exception);
    }

    TEST(UnusedEntity) {
        bool exception = false;
        try {
            ecs->local(unusedEnt);
        }
        catch (std::invalid_argument& e) {
            exception = true;
        }
        EXPECT(exception);
    }

    TEST(StoreAndRecall) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto comp = testComps[i];
            auto ent  = testEnts[i];
            auto localComp = ecs->local(comp);
            auto localEnt  = ecs->local(ent);

            int value = std::rand();
            ecs->get(ent, comp) = value;
            EXPECT(ecs->get(ent, comp) == value);
            EXPECT(ecs->get(localEnt, comp) == value);
            EXPECT(ecs->get(ent, localComp) == value);
            EXPECT(ecs->get(localEnt, localComp) == value);
        }
    }

    TEST(StoreAndRecallOwnership) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto comp = testComps[i];
            auto ent  = testEnts[i];
            auto localComp = ecs->local(comp);
            auto localEnt  = ecs->local(ent);

            bool value = std::rand() % 2 == 0;
            ecs->owns(ent, comp) = value;
            EXPECT(ecs->owns(ent, comp) == value);
            EXPECT(ecs->owns(localEnt, comp) == value);
            EXPECT(ecs->owns(ent, localComp) == value);
            EXPECT(ecs->owns(localEnt, localComp) == value);
        }
    }

    TEST(DefaultOwnership) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto comp = testComps[i];
            auto ent  = testEnts[i];
            auto localComp = ecs->local(comp);
            auto localEnt  = ecs->local(ent);

            ecs->get(ent, comp) = i;

            EXPECT(ecs->owns(ent, comp));
            EXPECT(ecs->owns(localEnt, comp));
            EXPECT(ecs->owns(ent, localComp));
            EXPECT(ecs->owns(localEnt, localComp));
        }
    }

    TEST(AdoptOverloads) {
        auto parent = testEnts[0];
        auto localParent = ecs->local(parent);
        for (int i = 1; i < REPEAT_TIMES; ++i) {
            auto ent = testEnts[i];
            auto localEnt = ecs->local(ent);

            ecs->orphan(ent) = true;
            ecs->adopt(ent, parent);
            EXPECT(!ecs->orphan(ent));

            ecs->orphan(ent) = true;
            ecs->adopt(localEnt, parent);
            EXPECT(!ecs->orphan(ent));

            ecs->orphan(ent) = true;
            ecs->adopt(ent, localParent);
            EXPECT(!ecs->orphan(ent));

            ecs->orphan(ent) = true;
            ecs->adopt(localEnt, localParent);
            EXPECT(!ecs->orphan(ent));
        }
    }

    TEST(DefaultOrphanValue) {
        auto parent = testEnts[0];
        for (int i = 1; i < REPEAT_TIMES; ++i) {
            auto ent  = testEnts[i];
            auto localEnt  = ecs->local(ent);

            ecs->orphan(ent) = true;

            EXPECT(ecs->orphan(ent));
            EXPECT(ecs->orphan(localEnt));

            ecs->adopt(ent, parent);

            EXPECT(!ecs->orphan(ent));
            EXPECT(!ecs->orphan(localEnt));
        }
    }

    TEST(ParentOwnership) {
        auto comp = testComps[0];
        auto parent = testEnts[0];

        ecs->owns(parent, comp) = false;
        for (int i = 1; i < REPEAT_TIMES; ++i) {;
            auto ent = testEnts[i];
            auto localEnt = ecs->local(ent);

            ecs->adopt(ent, parent);
            ecs->owns(ent, comp) = false;

            EXPECT(!ecs->has(ent, comp));
            EXPECT(!ecs->has(localEnt, comp));

            ecs->owns(ent, comp) = true;

            EXPECT(ecs->has(ent, comp));
            EXPECT(ecs->has(localEnt, comp));
        }

        ecs->owns(parent, comp) = true;
        for (int i = 1; i < REPEAT_TIMES; ++i) {;
            auto ent = testEnts[i];
            auto localEnt = ecs->local(ent);

            ecs->adopt(ent, parent);
            ecs->owns(ent, comp) = false;

            EXPECT(ecs->has(ent, comp));
            EXPECT(ecs->has(localEnt, comp));

            ecs->owns(ent, comp) = true;

            EXPECT(ecs->has(ent, comp));
            EXPECT(ecs->has(localEnt, comp));
        }
    }

    TEST(ParentComponentValues) {
        auto comp = testComps[0];
        auto parent = ecs->local(testEnts[0]);

        for (int i = 1; i < REPEAT_TIMES; ++i) {
            auto ent = testEnts[i];
            auto localEnt = ecs->local(ent);

            ecs->adopt(ent, parent);

            int parentValue = std::rand();
            ecs->get(parent, comp) = parentValue;
            ecs->owns(ent, comp) = false;

            EXPECT(ecs->view(ent, comp) == parentValue);
            EXPECT(ecs->view(localEnt, comp) == parentValue);

            int childValue = std::rand();
            ecs->get(ent, comp) = childValue;

            EXPECT(ecs->view(ent, comp) == childValue);
            EXPECT(ecs->view(localEnt, comp) == childValue);
        }
    }

    TEST(SimpleIteration) {
        auto comp = testComps[0];
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            ecs->get(testEnts[i], comp) = i;
        }

        bool found[REPEAT_TIMES] = {0};
        for (auto it = ecs->begin(); it != ecs->end(); ++it) {
            ASSERT(ecs->owns(*it, comp));
            found[ecs->get(*it, comp)] = true;
        }

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(found[i]);
        }
    }

    TEST(RestrictedIteration) {
        auto comp = testComps[0];
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            ecs->get(testEnts[i], comp) = i;
            ecs->owns(testEnts[i], comp) = (i % 2 == 0);
        }

        bool found[REPEAT_TIMES] = {0};
        for (auto it = ecs->begin(comp); it != ecs->end(); ++it) {
            ASSERT(ecs->owns(*it, comp));
            found[ecs->get(*it, comp)] = true;
        }

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(found[i] == (i % 2 == 0));
        }
    }

    TEST(MultipleRestrictedIteration) {
        auto comp1 = testComps[0];
        auto comp2 = testComps[1];
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto ent = testEnts[i];
            ecs->get(ent, comp1) = i;
            ecs->owns(ent, comp1) = (i % 2 == 0);       // T, F, T, F, T, F, ...
            ecs->owns(ent, comp2) = ((i / 2) % 2 == 0); // T, T, F, F, T, T, ...
        }

        bool found[REPEAT_TIMES] = {0};
        for (auto it = ecs->begin(comp1, comp2); it != ecs->end(); ++it) {
            ASSERT(ecs->owns(*it, comp1));
            EXPECT(ecs->owns(*it, comp2));
            found[ecs->get(*it, comp1)] = true;
        }

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(found[i] == (i % 4 == 0));
        }
    }

    TEST(ViewIteration) {
        auto comp = testComps[0];

        auto goodParent = testEnts[0];
        auto badParent  = testEnts[1];

        ecs->get(goodParent, comp) = 0;
        ecs->owns(badParent, comp) = false;

        for (int i = 2; i < REPEAT_TIMES; ++i) {
            auto ent = testEnts[i];

            ecs->get(ent, comp) = i;
            ecs->adopt(ent, (i % 2 == 0) ? goodParent : badParent); // T, F, T, F, T, F, ...
            ecs->owns(ent, comp) = ((i / 2) % 2 == 0);              // T, T, F, F, T, T, ...
        }

        bool found[REPEAT_TIMES] = {0};
        for (auto it = ecs->begin(comp).view(); it != ecs->end(); ++it) {
            if (*it == ecs->local(goodParent)) continue;

            ASSERT(ecs->has(*it, comp));
            int value = ecs->view(*it, comp);

            // value is 0 only if we inherit from goodParent
            if (value == 0) EXPECT(!ecs->owns(*it, comp));
            found[value] = true;
        }

        for (int i = 2; i < REPEAT_TIMES; ++i) {
            EXPECT(found[i] == ((i / 2) % 2  == 0));
        }
    }

    TEST(EmptyIterator) {
        auto comp = testComps[0];

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto ent = testEnts[i];
            ecs->owns(ent, comp) = false;
        }

        auto it = ecs->begin(comp);
        EXPECT(it == ecs->end());
    }

    TEST(MoveConstructor) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            ecs->get(testEnts[i], testComps[i]) = i;
        }

        EntityTable table(std::move(*ecs));

        EXPECT(&table != ecs);

        // New table should already have each component
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            bool exception = false;
            try {
                table.local(testComps[i]);
            }
            catch (std::invalid_argument& e) {
                exception = true;
            }
            ASSERT(!exception);
        }

        // New table should already have each entity
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            bool exception = false;
            try {
                table.local(testEnts[i]);
            }
            catch (std::invalid_argument& e) {
                exception = true;
            }
            ASSERT(!exception);
        }

        // Old data should be available in new table
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(table.owns(testEnts[i], testComps[i]));
            EXPECT(table.get(testEnts[i], testComps[i]) == i);
        }
    }

    TEST(ConstGetThrows) {
        auto ent  = testEnts[0];
        auto comp = testComps[0];
        ecs->owns(ent, comp) = false;

        bool exception = false;
        try {
            ecs->cGet(ent, comp);
        }
        catch (std::invalid_argument& e) {
            exception = true;
        }
        EXPECT(exception);
    }

    TEST(ConstViewThrows) {
        auto parent = testEnts[0];
        auto child  = testEnts[1];
        auto comp   = testComps[0];

        ecs->adopt(child, parent);
        ecs->owns(child, comp)  = false;
        ecs->owns(parent, comp) = false;

        bool exception = false;
        try {
            ecs->cView(child, comp);
        }
        catch (std::invalid_argument& e) {
            exception = true;
        }
        EXPECT(exception);
    }

    TEST(ConstRecall) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto ent  = testEnts[i];
            auto comp = testComps[i];
            auto localEnt  = ecs->local(ent);
            auto localComp = ecs->local(comp);

            int value = std::rand();
            ecs->get(ent, comp) = value;
            EXPECT(ecs->cGet(ent, comp) == value);
            EXPECT(ecs->cGet(localEnt, comp) == value);
            EXPECT(ecs->cGet(ent, localComp) == value);
            EXPECT(ecs->cGet(localEnt, localComp) == value);
        }
    }

    TEST(ConstRecallOwnership) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto ent  = testEnts[i];
            auto comp = testComps[i];
            auto localEnt  = ecs->local(ent);
            auto localComp = ecs->local(comp);

            bool value = std::rand() % 2 == 0;
            ecs->owns(ent, comp) = value;
            EXPECT(ecs->cOwns(ent, comp) == value);
            EXPECT(ecs->cOwns(localEnt, comp) == value);
            EXPECT(ecs->cOwns(ent, localComp) == value);
            EXPECT(ecs->cOwns(localEnt, localComp) == value);
        }
    }

    TEST(ConstParentComponentValues) {
        auto parent = testEnts[0];

        for (int i = 1; i < REPEAT_TIMES; ++i) {
            auto ent  = testEnts[i];
            auto comp = testComps[i];
            auto localEnt  = ecs->local(ent);
            auto localComp = ecs->local(comp);

            int childValue  = std::rand();
            int parentValue = std::rand();

            ecs->adopt(ent, parent);
            ecs->get(ent, comp) = childValue;
            ecs->get(parent, comp) = parentValue;

            EXPECT(ecs->cView(ent, comp) == childValue);
            EXPECT(ecs->cView(localEnt, comp) == childValue);
            EXPECT(ecs->cView(ent, localComp) == childValue);
            EXPECT(ecs->cView(localEnt, localComp) == childValue);

            ecs->owns(ent, comp) = false;

            EXPECT(ecs->cView(ent, comp) == parentValue);
            EXPECT(ecs->cView(localEnt, comp) == parentValue);
            EXPECT(ecs->cView(ent, localComp) == parentValue);
            EXPECT(ecs->cView(localEnt, localComp) == parentValue);
        }
    }
}
