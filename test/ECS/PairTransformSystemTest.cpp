#include "SuzuTest.hpp"

#include "ECS/PairTransformSystem.hpp"

#include <array>
#include <tuple>
#include <iostream>

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"
#include "ECS/EntityTable.hpp"

#define REPEAT_TIMES 20

using namespace Suzu;

typedef std::tuple<int, bool> T;
std::array<T, 2> isUniqueSystem(T in1, T in2) {
    auto& [value1, isUnique1] = in1;
    auto& [value2, isUnique2] = in2;

    if (value1 == value2) {
        isUnique1 = false;
        isUnique2 = false;
    }

    return {in1, in2};
}

SUITE(EcsPairTransformSystem) {
    EntityTable* table;
    SystemBase* system;

    Entity ents[REPEAT_TIMES];
    Component<int> valueComp;
    Component<bool> isUniqueComp;

    SETUP {
        table = new EntityTable;
        table->use(valueComp);
        table->use(isUniqueComp);
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->use(ents[i]);
        }
        system = new PairTransformSystem(*table, isUniqueSystem, valueComp, isUniqueComp);
    }

    TEARDOWN {
        delete system;
        delete table;
    }

    TEST(Success) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->get(ents[i], valueComp) = i;
            table->get(ents[i], isUniqueComp) = true;
        }

        system->apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(table->get(ents[i], isUniqueComp));
        }
    }

    TEST(Failure) {
        for (int i = 1; i < REPEAT_TIMES; i += 2) {
            table->get(ents[i], valueComp) = i;
            table->get(ents[i], isUniqueComp) = true;

            table->get(ents[i-1], valueComp) = i;
            table->get(ents[i-1], isUniqueComp) = true;
        }

        system->apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(!table->get(ents[i], isUniqueComp));
        }
    }

    TEST(PartialApplication) {
        for (int i = 1; i < REPEAT_TIMES; i += 2) {
            table->get(ents[i], valueComp) = i;
            table->get(ents[i], isUniqueComp) = true;

            table->get(ents[i-1], valueComp) = i;
            table->owns(ents[i-1], isUniqueComp) = false;
        }

        system->apply();

        for (int i = 1; i < REPEAT_TIMES; i += 2) {
            EXPECT(table->get(ents[i], isUniqueComp));
        }
    }

    TEST(VoidComponents) {
        Component<void> flag;
        table->use(flag);
        PairTransformSystem flagSystem(*table, isUniqueSystem, valueComp, isUniqueComp, flag);
        for (int i = 1; i < REPEAT_TIMES; i += 2) {
            table->get(ents[i], valueComp) = i;
            table->get(ents[i], isUniqueComp) = true;
            table->owns(ents[i], flag) = true;

            table->get(ents[i-1], valueComp) = i;
            table->get(ents[i-1], isUniqueComp) = true;
            table->owns(ents[i-1], flag) = false;
        }

        flagSystem.apply();

        for (int i = 1; i < REPEAT_TIMES; i += 2) {
            EXPECT(table->get(ents[i], isUniqueComp));
        }
    }
}
