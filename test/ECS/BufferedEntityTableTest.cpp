#include "SuzuTest.hpp"

#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <utility>

#include "ECS/BufferedEntityTable.hpp"

#define REPEAT_TIMES 20

using namespace Suzu;

SUITE(EcsBufferedEntityTable) {
    BufferedEntityTable* ecs;

    Entity testEnts[REPEAT_TIMES];
    Component<int> testComps[REPEAT_TIMES];

    Entity unusedEnt;
    Component<int> unusedComp;

    SETUP {
        std::srand(std::time(nullptr));
        ecs = new BufferedEntityTable;

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            ecs->use(testEnts[i]);
            ecs->use(testComps[i]);
        }
    }

    TEARDOWN {
        delete ecs;
    }

    TEST(Buffer) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto ent  = testEnts[i];
            auto comp = testComps[i];

            ecs->owns(ent, comp) = true;
            ecs->get(ent, comp) = 0;
            ecs->set(ent, comp, i);
        }

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto ent  = testEnts[i];
            auto comp = testComps[i];
            EXPECT(ecs->get(ent, comp) == 0);
        }

        ecs->swap();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            auto ent  = testEnts[i];
            auto comp = testComps[i];
            EXPECT(ecs->get(ent, comp) == i);
        }
    }

    TEST(MoveConstructor) {
        // Store 2*i in next buffer, i in current buffer
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            ecs->get(testEnts[i], testComps[i]) = 2 * i;
        }
        ecs->swap();
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            ecs->get(testEnts[i], testComps[i]) = i;
        }

        BufferedEntityTable table(std::move(*ecs));

        // New table should already have each component
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            bool exception = false;
            try {
                table.local(testComps[i]);
            }
            catch (std::invalid_argument& e) {
                exception = true;
            }
            ASSERT(!exception);
        }

        // New table should already have each entity
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            bool exception = false;
            try {
                table.local(testEnts[i]);
            }
            catch (std::invalid_argument& e) {
                exception = true;
            }
            ASSERT(!exception);
        }

        // Old data should be available in new table
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(table.owns(testEnts[i], testComps[i]));
            EXPECT(table.get(testEnts[i], testComps[i]) == i);
        }
        table.swap();
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(table.owns(testEnts[i], testComps[i]));
            EXPECT(table.get(testEnts[i], testComps[i]) == 2 * i);
        }
    }
}
