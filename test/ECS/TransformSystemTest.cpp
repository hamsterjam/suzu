#include "SuzuTest.hpp"

#include "ECS/TransformSystem.hpp"

#include <tuple>

#include "ECS/BufferedEntityTable.hpp"
#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"
#include "ECS/EntityTable.hpp"

#define REPEAT_TIMES 20

using namespace Suzu;

std::tuple<int, float> doubleSystem(std::tuple<int, float> in) {
    return std::make_tuple(
        std::get<0>(in) * 2,
        std::get<1>(in) * 2
    );
}

SUITE(EcsTransformSystem) {
    EntityTable* table;
    BufferedEntityTable* bufferedTable;
    SystemBase* system;
    SystemBase* bufferedSystem;

    Entity ents[REPEAT_TIMES];
    Component<int> comp1;
    Component<float> comp2;

    SETUP {
        table = new EntityTable;
        bufferedTable = new BufferedEntityTable;

        table->use(comp1);
        table->use(comp2);
        bufferedTable->use(comp1);
        bufferedTable->use(comp2);
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->use(ents[i]);
            bufferedTable->use(ents[i]);
        }
        system = new TransformSystem(*table, doubleSystem, comp1, comp2);
        bufferedSystem = new TransformSystem(*bufferedTable, doubleSystem, comp1, comp2);
    }

    TEARDOWN {
        delete table;
        delete bufferedTable;
        delete system;
        delete bufferedSystem;
    }

    TEST(Behaviour) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->get(ents[i], comp1) = i;
            table->get(ents[i], comp2) = i;
        }

        system->apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(table->get(ents[i], comp1) == i * 2);
            EXPECT(table->get(ents[i], comp2) == (float) i * 2);
        }
    }

    TEST(Buffered) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            bufferedTable->get(ents[i], comp1) = i;
            bufferedTable->get(ents[i], comp2) = i;
        }

        bufferedSystem->apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(bufferedTable->get(ents[i], comp1) == i);
            EXPECT(bufferedTable->get(ents[i], comp2) == (float)i);
        }

        bufferedTable->swap();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(bufferedTable->get(ents[i], comp1) == i * 2);
            EXPECT(bufferedTable->get(ents[i], comp2) == (float)i * 2);
        }
    }

    TEST(BufferedAsUnbuffered) {
        BufferedEntityTable& testTable = *bufferedTable;
        TransformSystem testSystem((EntityTable&)testTable, doubleSystem, comp1, comp2);

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            testTable.get(ents[i], comp1) = i;
            testTable.get(ents[i], comp2) = i;
        }

        testSystem.apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(testTable.get(ents[i], comp1) == i * 2);
            EXPECT(testTable.get(ents[i], comp2) == (float)i * 2);
        }
    }

    TEST(PartialApplication) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->get(ents[i], comp1) = i;
            table->get(ents[i], comp2) = i;

            table->owns(ents[i], comp1) = (i % 2 == 0);       // T, F, T, F, T, F, ...
            table->owns(ents[i], comp2) = ((i / 2) % 2 == 0); // T, T, F, F, T, T, ...
        }

        system->apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            bool owns1 = table->owns(ents[i], comp1);
            bool owns2 = table->owns(ents[i], comp2);

            if (owns1 && owns2) {
                EXPECT(table->get(ents[i], comp1) == i * 2);
                EXPECT(table->get(ents[i], comp2) == (float)i * 2);
            }
            else if (owns1) {
                EXPECT(table->get(ents[i], comp1) == i);
            }
            else if (owns2) {
                EXPECT(table->get(ents[i], comp2) == (float)i);
            }
        }
    }

    TEST(VoidComponents) {
        Component<void> flag1;
        Component<void> flag2;
        table->use(flag1);
        table->use(flag2);

        TransformSystem flagSystem(*table, doubleSystem, flag1, comp1, flag2, comp2);

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->get(ents[i], comp1) = i;
            table->get(ents[i], comp2) = i;

            table->owns(ents[i], flag1) = ((i % 2) == 0);     // T, F, T, F, T, F
            table->owns(ents[i], flag2) = ((i / 2) % 2 == 0); // T, T, F, F, T, T
        }

        flagSystem.apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            bool owns1 = table->owns(ents[i], flag1);
            bool owns2 = table->owns(ents[i], flag2);

            if (owns1 && owns2) {
                EXPECT(table->get(ents[i], comp1) == i * 2);
                EXPECT(table->get(ents[i], comp2) == (float)i * 2);
            }
            else {
                EXPECT(table->get(ents[i], comp1) == i);
                EXPECT(table->get(ents[i], comp2) == (float)i);
            }
        }
    }
}
