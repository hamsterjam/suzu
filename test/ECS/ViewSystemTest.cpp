#include "SuzuTest.hpp"

#include "ECS/ViewSystem.hpp"

#include <tuple>

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"
#include "ECS/EntityTable.hpp"

#define REPEAT_TIMES 20

using namespace Suzu;

bool visited[REPEAT_TIMES];
void checkSystem(std::tuple<int> in) {
    auto [value] = in;
    visited[value] = true;
}

SUITE(EcsViewSystem) {
    EntityTable* table;
    SystemBase* system;

    Entity ents[REPEAT_TIMES];
    Component<int> comp;

    SETUP {
        table = new EntityTable;

        table->use(comp);
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->use(ents[i]);
        }

        system = new ViewSystem(*table, checkSystem, comp);

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            visited[i] = false;
        }
    }

    TEARDOWN {
        delete system;
        delete table;
    }

    TEST(Behavior) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->get(ents[i], comp) = i;
        }

        system->apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(visited[i]);
        }
    }

    TEST(PartialApplication) {
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->get(ents[i], comp) = i;
            table->owns(ents[i], comp) = (i % 2 == 0);
        }

        system->apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(visited[i] == (i % 2 == 0));
        }
    }

    TEST(ParentedView) {
        for (int i = 1; i < REPEAT_TIMES; i += 2) {
            auto parent = ents[i];
            auto child  = ents[i - 1];

            table->adopt(child, parent);
            table->owns(child, comp) = false;
            table->get(parent, comp) = i;
        }

        system->apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(visited[i] == (i % 2 == 1));
        }
    }

    TEST(VoidComponents) {
        Component<void> flag;
        table->use(flag);
        ViewSystem flagSystem(*table, checkSystem, comp, flag);
        for (int i = 0; i < REPEAT_TIMES; ++i) {
            table->get(ents[i], comp) = i;
            table->owns(ents[i], flag) = (i % 2 == 0);
        }

        flagSystem.apply();

        for (int i = 0; i < REPEAT_TIMES; ++i) {
            EXPECT(visited[i] == (i % 2 == 0));
        }
    }
}
