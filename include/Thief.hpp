#ifndef SUZU_THIEF_HPP_DEFINED_
#define SUZU_THIEF_HPP_DEFINED_

// This doesn't seem to work if it's in a namespace...
// Should probably look into that
template <typename Tag, typename Tag::type M>
struct SuzuThief {
    friend typename Tag::type suzuSteal(Tag) {
        return M;
    }
};

#define SUZU_THIEF_GET_5TH_ARG(arg1, arg2, arg3, arg4, arg5, ...) arg5

#define SUZU_THIEF_STRUCT_NAME(identifier, member) \
    Thief_##identifier##_##member

#define SUZU_THIEF_MAKE_THIEF_4(identifier, class, typeidentifier, member) \
    struct SUZU_THIEF_STRUCT_NAME(identifier, member) { \
        typedef typeidentifier class::*type; \
        friend type suzuSteal(SUZU_THIEF_STRUCT_NAME(identifier, member)); \
    }; \
    template struct SuzuThief<SUZU_THIEF_STRUCT_NAME(identifier, member), &class::member>; \

#define SUZU_THIEF_MAKE_THIEF_3(class, typename, member) \
    SUZU_THIEF_MAKE_THIEF_4(class, class, typename, member)

#define SUZU_THIEF_CHOOSE_MAKE_THIEF_MACRO(...) \
    SUZU_THIEF_GET_5TH_ARG(__VA_ARGS__, SUZU_THIEF_MAKE_THIEF_4, SUZU_THIEF_MAKE_THIEF_3)

#define SUZU_THIEF_MAKE_THIEF(...) SUZU_THIEF_CHOOSE_MAKE_THIEF_MACRO(__VA_ARGS__)(__VA_ARGS__)

#define SUZU_THIEF_STEAL(variable, identifier, member) \
    (variable).*suzuSteal(SUZU_THIEF_STRUCT_NAME(identifier, member)())

#ifndef SUZU_THIEF_NO_GENERIC_DEFINES
#define MAKE_THIEF SUZU_THIEF_MAKE_THIEF
#define STEAL SUZU_THIEF_STEAL
#endif

#endif
