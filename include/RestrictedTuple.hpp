#ifndef SUZU_RESTRICTED_TUPLE_HPP_DEFINED_
#define SUZU_RESTRICTED_TUPLE_HPP_DEFINED_

#include <type_traits>
#include <tuple>

namespace Suzu {
    namespace Impl {
        template <typename... Ts>
        using TupleCatType = decltype(std::tuple_cat(std::declval<Ts>()...));
    }

    template <typename T, typename... Ts>
    using RestrictedTuple = Impl::TupleCatType<
        typename std::conditional_t<
            std::is_same_v<T, Ts>,
            std::tuple<>,
            std::tuple<Ts>
        >...
    >;

    template <typename U>
    std::tuple<> makeRestrictedTuple() noexcept {
        return std::tuple<>{};
    }

    template <typename U, typename T, typename... Ts>
    RestrictedTuple<U, T, Ts...> makeRestrictedTuple(T first, Ts... values) noexcept {
        if constexpr (std::is_same_v<U, T>) {
            return makeRestrictedTuple<U>(values...);
        }
        else {
            return std::tuple_cat(std::make_tuple(first), makeRestrictedTuple<U>(values...));
        }
    }
}

#endif
