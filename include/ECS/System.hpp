#ifndef SUZU_ECS_SYSTEM_HPP_DEFINED_
#define SUZU_ECS_SYSTEM_HPP_DEFINED_

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"
#include "RestrictedTuple.hpp"

#include <tuple>
#include <type_traits>

namespace Suzu {
    class EntityTable;
    class BufferedEntityTable;

    class SystemBase {
        public:
            virtual void apply() noexcept = 0;
    };

    template <typename E, typename... Ts>
    requires std::is_base_of_v<EntityTable, E>
    class System : public SystemBase {
        protected:
            E* table;
            std::tuple<LocalComponent<Ts>...> comps;
            RestrictedTuple<LocalComponent<void>, LocalComponent<Ts>...> nonVoidComps;

        public:
            System(E& table, Component<Ts>... comps);

        protected:
            template <typename T>
            void set(LocalEntity ent, LocalComponent<T> comp, const T& value) noexcept;

            RestrictedTuple<void, Ts...> getAll(LocalEntity ent) const;
            RestrictedTuple<void, Ts...> viewAll(LocalEntity ent) const;
            void setAll(LocalEntity ent, const RestrictedTuple<void, Ts...>& values) noexcept;

            typename E::Iterator getIterator() const noexcept;
    };

    template <typename E, typename... Ts>
    System<E, Ts...>::System(E& table, Component<Ts>... comps) {
        this->table = &table;
        this->comps = std::make_tuple(table.local(comps)...);
        this->nonVoidComps = makeRestrictedTuple<LocalComponent<void>>(table.local(comps)...);
    }

    template <typename E, typename... Ts>
    template <typename T>
    void System<E, Ts...>::set(LocalEntity ent, LocalComponent<T> comp, const T& value) noexcept {
        if constexpr (std::is_same_v<BufferedEntityTable, E>) {
            table->set(ent, comp, value);
        }
        else {
            table->get(ent, comp) = value;
        }
    }

    template <typename E, typename... Ts>
    RestrictedTuple<void, Ts...> System<E, Ts...>::getAll(LocalEntity ent) const {
        // This looks confusing, but it is just enumerating a tuple.
        // It simply calls table->cGet(ent, std::get<N>(nonVoidComps)) for each
        // valid value of N (and then returns the result as a tuple)
        return [this, ent]<std::size_t... Ns>(std::index_sequence<Ns...> seq) {
            return std::make_tuple(table->cGet(ent, std::get<Ns>(nonVoidComps))...);
        }(std::make_index_sequence<std::tuple_size_v<decltype(nonVoidComps)>>{});
    }

    template <typename E, typename... Ts>
    RestrictedTuple<void, Ts...> System<E, Ts...>::viewAll(LocalEntity ent) const {
        // This looks confusing, but it is just enumerating a tuple.
        // It simply calls table->cView(ent, std::get<N>(nonVoidComps)) for each
        // valid value of N (and then returns the result as a tuple)
        return [this, ent]<std::size_t... Ns>(std::index_sequence<Ns...> seq) {
            return std::make_tuple(table->cView(ent, std::get<Ns>(nonVoidComps))...);
        }(std::make_index_sequence<std::tuple_size_v<decltype(nonVoidComps)>>{});
    }

    template <typename E, typename... Ts>
    void System<E, Ts...>::setAll(LocalEntity ent, const RestrictedTuple<void, Ts...>& values) noexcept {
        // This looks confusing, but it is just enumerating a tuple.
        // It simply calls set(ent, std::get<N>(nonVoidComps), std::get<N>(values))
        // for each valid value of N
        [this, ent, values]<std::size_t... Ns>(std::index_sequence<Ns...> seq) {
            (this->set(ent, std::get<Ns>(this->nonVoidComps), std::get<Ns>(values)),...);
        }(std::make_index_sequence<std::tuple_size_v<decltype(nonVoidComps)>>{});
    }

    template <typename E, typename... Ts>
    typename E::Iterator System<E, Ts...>::getIterator() const noexcept {
        return std::apply(
            [this](LocalComponent<Ts>... comps) {
                return table->begin(comps...);
            },
            comps
        );
    }
}

#endif
