#ifndef SUZU_ECS_PAIR_TRANSFORM_SYSTEM_HPP_DEFINED_
#define SUZU_ECS_PAIR_TRANSFORM_SYSTEM_HPP_DEFINED_

#include "ECS/System.hpp"

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"
#include "RestrictedTuple.hpp"

#include <array>
#include <tuple>
#include <type_traits>

namespace Suzu {
    template <typename E, typename Fn, typename... Ts>
    requires std::is_invocable_r_v<std::array<RestrictedTuple<void, Ts...>, 2>, Fn, RestrictedTuple<void, Ts...>, RestrictedTuple<void, Ts...>>
    class PairTransformSystem : public System<E, Ts...> {
        private:
            Fn action;

        public:
            PairTransformSystem(E& table, Fn action, Component<Ts>... comps);
            virtual void apply() noexcept;
    };

    template <typename E, typename Fn, typename... Ts>
    PairTransformSystem<E, Fn, Ts...>::PairTransformSystem(E& table, Fn action, Component<Ts>... comps):
        System<E, Ts...>(table, comps...)
    {
        this->action = action;
    }

    template <typename E, typename Fn, typename... Ts>
    void PairTransformSystem<E, Fn, Ts...>::apply() noexcept {
        // noexcept is correct here despite getAll not being noexcept.
        // getAll will throw if an entity does not possess a component, but the iterator
        // ensures that all entities possess all the required components.
        for (auto it1 = this->getIterator(); it1 != this->table->end(); ++it1) {
            typename E::Iterator it2 = it1;
            ++it2;
            for (; it2 != this->table->end(); ++it2) {
                std::array<RestrictedTuple<void, Ts...>, 2> res = action(this->getAll(*it1), this->getAll(*it2));
                this->setAll(*it1, res[0]);
                this->setAll(*it2, res[1]);
            }
        }
    }
}

#endif
