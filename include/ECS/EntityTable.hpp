#ifndef SUZU_ECS_ENTITY_TABLE_HPP_DEFINED_
#define SUZU_ECS_ENTITY_TABLE_HPP_DEFINED_

#include <cstdlib>
#include <cstring>
#include <map>
#include <stdexcept>
#include <type_traits>
#include <utility>
#include <vector>

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"

namespace Suzu {
    class EntityTable {
        private:
            std::map<unsigned int, unsigned int> entLocalId;
            std::map<unsigned int, unsigned int> compLocalId;

        protected:
            struct EntMeta {
                bool orphan;
                LocalEntity parent;
            };
            struct CompMeta {
                std::size_t size;
            };
            struct ValueMeta {
                bool exists;
            };

            EntMeta*    entMetadata;
            CompMeta*   compMetadata;
            ValueMeta** valueMetadata;

            unsigned int maxEnts;
            unsigned int maxComps;

            unsigned int nextEnt;
            unsigned int nextComp;

            void** data;

        public:
            class Iterator;

            EntityTable() noexcept;
            ~EntityTable() noexcept;

            EntityTable(const EntityTable& other) = delete; // Disable copy constructor
            EntityTable(EntityTable&& other) noexcept; // Move constructor

            EntityTable& operator=(const EntityTable& other) = delete; // Disable copy assignment
            EntityTable& operator=(EntityTable&& other) noexcept; // Move assignment

            template <typename T>
            void use(Component<T> comp) noexcept;
            void use(Entity ent) noexcept;

            template <typename T>
            LocalComponent<T> local(Component<T> comp) const;
            LocalEntity local(Entity ent) const;

            template <typename T>
            requires (!std::is_void_v<T>)
            T& get(LocalEntity ent, LocalComponent<T> comp) noexcept;
            template <typename T>
            T& get(Entity ent, LocalComponent<T> comp) { return get(local(ent), comp); }
            template <typename T>
            T& get(LocalEntity ent, Component<T> comp) { return get(ent, local(comp)); }
            template <typename T>
            T& get(Entity ent, Component<T> comp) { return get(local(ent), local(comp)); }

            template <typename T>
            requires (!std::is_void_v<T>)
            const T& cGet(LocalEntity ent, LocalComponent<T> comp) const;
            template <typename T>
            const T& cGet(Entity ent, LocalComponent<T> comp) const { return cGet(local(ent), comp); }
            template <typename T>
            const T& cGet(LocalEntity ent, Component<T> comp) const { return cGet(ent, local(comp)); }
            template <typename T>
            const T& cGet(Entity ent, Component<T> comp) const { return cGet(local(ent), local(comp)); }

            template <typename T>
            requires (!std::is_void_v<T>)
            const T& view(LocalEntity ent, LocalComponent<T> comp) noexcept;
            template <typename T>
            const T& view(Entity ent, LocalComponent<T> comp) { return view(local(ent), comp); }
            template <typename T>
            const T& view(LocalEntity ent, Component<T> comp) { return view(ent, local(comp)); }
            template <typename T>
            const T& view(Entity ent, Component<T> comp) { return view(local(ent), local(comp)); }

            template <typename T>
            requires (!std::is_void_v<T>)
            const T& cView(LocalEntity ent, LocalComponent<T> comp) const;
            template <typename T>
            const T& cView(Entity ent, LocalComponent<T> comp) const { return cView(local(ent), comp); }
            template <typename T>
            const T& cView(LocalEntity ent, Component<T> comp) const { return cView(ent, local(comp)); }
            template <typename T>
            const T& cView(Entity ent, Component<T> comp) const { return cView(local(ent), local(comp)); }

            bool& owns(LocalEntity ent, LocalComponent<void> comp) noexcept;
            bool& owns(Entity ent, LocalComponent<void> comp) { return owns(local(ent), comp); }
            bool& owns(LocalEntity ent, Component<void> comp) { return owns(ent, local(comp)); }
            bool& owns(Entity ent, Component<void> comp) { return owns(local(ent), local(comp)); }

            bool cOwns(LocalEntity ent, LocalComponent<void> comp) const noexcept;
            bool cOwns(Entity ent, LocalComponent<void> comp) const { return cOwns(local(ent), comp); }
            bool cOwns(LocalEntity ent, Component<void> comp) const { return cOwns(ent, local(comp)); }
            bool cOwns(Entity ent, Component<void> comp) const { return cOwns(local(ent), local(comp)); }

            bool has(LocalEntity ent, LocalComponent<void> comp) const noexcept;
            bool has(Entity ent, LocalComponent<void> comp) const { return has(local(ent), comp); }
            bool has(LocalEntity ent, Component<void> comp) const { return has(ent, local(comp)); }
            bool has(Entity ent, Component<void> comp) const { return has(local(ent), local(comp)); }

            void adopt(LocalEntity child, LocalEntity parent) noexcept;
            void adopt(Entity child, LocalEntity parent) { adopt(local(child), parent); }
            void adopt(LocalEntity child, Entity parent) { adopt(child, local(parent)); }
            void adopt(Entity child, Entity parent) { adopt(local(child), local(parent)); }

            bool& orphan(LocalEntity ent) noexcept;
            bool& orphan(Entity ent) { return orphan(local(ent)); }

            Iterator begin() const noexcept;
            Iterator end() const noexcept;

            template <typename... Ts>
            Iterator begin(LocalComponent<void> comp, Ts... args) const;
            template <typename... Ts>
            Iterator begin(Component<void> comp, Ts... args) const;

        protected:
            void clean() noexcept;

            virtual void createEmptyCompTable(unsigned int id, std::size_t size) noexcept;
            virtual void doubleMaxEnts() noexcept;
            virtual void doubleMaxComps() noexcept;

            static unsigned int getEntId(LocalEntity ent) noexcept;
            static unsigned int getCompId(LocalComponent<void> comp) noexcept;
    };

    class EntityTable::Iterator {
        private:
            friend class EntityTable;
            friend bool operator==(const Iterator& lhs, const Iterator& rhs) noexcept;

            const EntityTable* table;
            LocalEntity currEnt;
            std::vector<LocalComponent<void>> requirements;

            bool useHas = false;

        public:
            Iterator() = delete;

            void require(LocalComponent<void> comp) noexcept;
            Iterator& view(bool view = true) noexcept;

            Iterator& operator++() noexcept;
            LocalEntity operator*() noexcept;

        private:
            Iterator(LocalEntity ent, const EntityTable& table) noexcept;

            bool valid() noexcept;
    };

    bool operator==(const EntityTable::Iterator& lhs, const EntityTable::Iterator& rhs) noexcept;
    bool operator!=(const EntityTable::Iterator& lhs, const EntityTable::Iterator& rhs) noexcept;

    template <typename T>
    void EntityTable::use(Component<T> comp) noexcept {
        if (compLocalId.count(comp.id) != 0) return;
        compLocalId[comp.id] = nextComp;

        if (nextComp >= maxComps) doubleMaxComps();
        createEmptyCompTable(nextComp, sizeof(T));

        valueMetadata[nextComp] = (ValueMeta*) std::malloc(sizeof(ValueMeta) * maxEnts);
        std::memset(valueMetadata[nextComp], 0, sizeof(ValueMeta) * maxEnts);
        compMetadata[nextComp].size = sizeof(T);

        ++nextComp;
    }

    template <>
    void EntityTable::use(Component<void> comp) noexcept;

    template <typename T>
    LocalComponent<T> EntityTable::local(Component<T> comp) const {
        if (!compLocalId.contains(comp.id)) {
            throw std::invalid_argument("EntityTable::local(Component<T>): EntityTable does not use Component!");
        }
        auto it = compLocalId.find(comp.id);
        return LocalComponent<T>(it->second);
    }

    template <typename T>
    requires (!std::is_void_v<T>)
    T& EntityTable::get(LocalEntity ent, LocalComponent<T> comp) noexcept {
        owns(ent, comp) = true;
        auto table = (T*) data[comp.id];
        return table[ent.id];
    }

    template <typename T>
    requires (!std::is_void_v<T>)
    const T& EntityTable::cGet(LocalEntity ent, LocalComponent<T> comp) const {
        if (!cOwns(ent, comp)) {
            throw std::invalid_argument("EntityTable::cGet(LocalEntity ent, LocalComponent<T>): Entity does not possess Component!");
        }
        auto table = (T*) data[comp.id];
        return table[ent.id];
    }

    template <typename T>
    requires (!std::is_void_v<T>)
    const T& EntityTable::view(LocalEntity ent, LocalComponent<T> comp) noexcept {
        if (orphan(ent) || owns(ent, comp)) return get(ent, comp);
        return view(entMetadata[ent.id].parent, comp);
    }

    template <typename T>
    requires (!std::is_void_v<T>)
    const T& EntityTable::cView(LocalEntity ent, LocalComponent<T> comp) const {
        bool exists = valueMetadata[comp.id][ent.id].exists;
        bool orphan = entMetadata[ent.id].orphan;
        if (orphan || exists) return cGet(ent, comp);
        return cView(entMetadata[ent.id].parent, comp);
    }

    template <typename... Ts>
    EntityTable::Iterator EntityTable::begin(LocalComponent<void> comp, Ts... args) const {
        Iterator it = std::move(begin(args...));
        it.require(comp);
        return it;
    }

    template <typename... Ts>
    EntityTable::Iterator EntityTable::begin(Component<void> comp, Ts... args) const {
        return begin(local(comp), args...);
    }
}

#endif
