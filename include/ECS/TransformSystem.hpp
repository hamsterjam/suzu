#ifndef SUZU_ECS_TRANSFORM_SYSTEM_HPP_DEFINED_
#define SUZU_ECS_TRANSFORM_SYSTEM_HPP_DEFINED_

#include "ECS/System.hpp"

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"
#include "RestrictedTuple.hpp"

#include <tuple>
#include <type_traits>

namespace Suzu {
    template <typename E, typename Fn, typename... Ts>
    requires std::is_invocable_r_v<RestrictedTuple<void, Ts...>, Fn, RestrictedTuple<void, Ts...>>
    class TransformSystem : public System<E, Ts...> {
        private:
            Fn action;

        public:
            TransformSystem(E& table, Fn action, Component<Ts>... comps);
            virtual void apply() noexcept;
    };

    template <typename E, typename Fn, typename... Ts>
    TransformSystem<E, Fn, Ts...>::TransformSystem(E& table, Fn action, Component<Ts>... comps):
        System<E, Ts...>(table, comps...)
    {
        this->action = action;
    }

    template <typename E, typename Fn, typename... Ts>
    void TransformSystem<E, Fn, Ts...>::apply() noexcept {
        // noexcept is correct here despite getAll not being noexcept.
        // getAll will throw if an entity does not possess a component, but the iterator
        // ensures that all entities possess all the required components.
        for (auto it = this->getIterator(); it != this->table->end(); ++it) {
            this->setAll(*it, action(this->getAll(*it)));
        }
    }
}

#endif
