#ifndef SUZU_ECS_COMPONENT_HPP_DEFINED_
#define SUZU_ECS_COMPONENT_HPP_DEFINED_

namespace Suzu {
    class EntityTable;

    template <typename T>
    class LocalComponent {
        private:
            template <typename U>
            friend class LocalComponent;
            friend class EntityTable;

            unsigned int id;

            LocalComponent(unsigned int id) noexcept {
                this->id = id;
            }

        public:
            LocalComponent() = default;

            operator LocalComponent<void>() noexcept {
                return LocalComponent<void>(id);
            }
    };

    class ComponentBase {
        protected:
            static unsigned int nextId;
    };

    template <typename T>
    class Component : public ComponentBase {
        private:
            template <typename U>
            friend class Component;
            friend class EntityTable;

            unsigned int id;

            Component(unsigned int id) noexcept {
                this->id = id;
            }

        public:
            Component() noexcept {
                id = nextId;
                ++nextId;
            }

            operator Component<void>() noexcept {
                return Component<void>(id);
            }
    };
}

#endif
