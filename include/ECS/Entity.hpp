#ifndef SUZU_ECS_ENTITY_HPP_DEFINED_
#define SUZU_ECS_ENTITY_HPP_DEFINED_

namespace Suzu {
    class EntityTable;

    class LocalEntity {
        private:
            friend class EntityTable;
            friend bool operator==(const LocalEntity&, const LocalEntity&) noexcept;
            unsigned int id;

            LocalEntity(unsigned int id) noexcept;

        public:
            LocalEntity() = default;
    };

    bool operator==(const LocalEntity& lhs, const LocalEntity& rhs) noexcept;
    bool operator!=(const LocalEntity& lhs, const LocalEntity& rhs) noexcept;

    class Entity {
        private:
            friend class EntityTable;
            static unsigned int nextId;
            unsigned int id;

        public:
            Entity() noexcept;
    };
}

#endif
