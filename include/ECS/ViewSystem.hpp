#ifndef SUZU_ECS_VIEW_SYSTEM_HPP_DEFINED_
#define SUZU_ECS_VIEW_SYSTEM_HPP_DEFINED_

#include "ECS/System.hpp"

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"
#include "RestrictedTuple.hpp"

#include <tuple>
#include <type_traits>

// As this does not modify the underlying EntityTable, it might be a good idea
// to define a new const System class, this is fine for now though

namespace Suzu {
    template <typename E, typename Fn, typename... Ts>
    requires std::is_invocable_v<Fn, RestrictedTuple<void, Ts...>>
    class ViewSystem : public System<E, Ts...> {
        private:
            Fn action;

        public:
            ViewSystem(E& table, Fn action, Component<Ts>... comps);
            virtual void apply() noexcept;
    };

    template <typename E, typename Fn, typename... Ts>
    ViewSystem<E, Fn, Ts...>::ViewSystem(E& table, Fn action, Component<Ts>... comps):
        System<E, Ts...>(table, comps...)
    {
        this->action = action;
    }

    template <typename E, typename Fn, typename... Ts>
    void ViewSystem<E, Fn, Ts...>::apply() noexcept {
        // noexcept is correct here despite viewAll not being noexcept.
        // getAll will throw if an entity does not possess a component, but the iterator
        // ensures that all entities possess all the required components.
        for (auto it = this->getIterator().view(); it != this->table->end(); ++it) {
            action(this->viewAll(*it));
        }
    }
}

#endif
