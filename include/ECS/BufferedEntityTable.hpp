#ifndef SUZU_ECS_BUFFERED_ENTITY_TABLE_HPP_DEFINED_
#define SUZU_ECS_BUFFERED_ENTITY_TABLE_HPP_DEFINED_

#include "ECS/EntityTable.hpp"

#include "ECS/Component.hpp"
#include "ECS/Entity.hpp"

namespace Suzu {
    class BufferedEntityTable : public EntityTable {
        private:
            void** dataBuffer;

        public:
            BufferedEntityTable() noexcept;
            ~BufferedEntityTable() noexcept;

            BufferedEntityTable(const BufferedEntityTable& other) = delete; // Disable copy constructor
            BufferedEntityTable(BufferedEntityTable&& other) noexcept; // Move constructor

            BufferedEntityTable& operator=(const BufferedEntityTable& other) = delete; // Disable copy assignment
            BufferedEntityTable& operator=(BufferedEntityTable&& other) noexcept;      // Move assignment

            template <typename T>
            void set(LocalEntity ent, LocalComponent<T> comp, const T& value) noexcept;
            template <typename T>
            void set(Entity ent, LocalComponent<T> comp, const T& value) { set(local(ent), comp, value); }
            template <typename T>
            void set(LocalEntity ent, Component<T> comp, const T& value) { set(ent, local(comp), value); }
            template <typename T>
            void set(Entity ent, Component<T> comp, const T& value) { set(local(ent), local(comp), value); }

            void swap() noexcept;

        protected:
            void clean() noexcept;

            virtual void createEmptyCompTable(unsigned int id, std::size_t size) noexcept;
            virtual void doubleMaxEnts() noexcept;
            virtual void doubleMaxComps() noexcept;
    };

    template <typename T>
    void BufferedEntityTable::set(LocalEntity ent, LocalComponent<T> comp, const T& value) noexcept {
        auto table = (T*) dataBuffer[getCompId(comp)];
        table[getEntId(ent)] = value;
    }
}

#endif
