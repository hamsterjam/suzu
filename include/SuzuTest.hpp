#ifndef SUZU_SUZU_TEST_HPP_DEFINED_
#define SUZU_SUZU_TEST_HPP_DEFINED_

#include <deque>
#include <map>
#include <sstream>
#include <string>

namespace SuzuTest {
    class TestBase;
    typedef std::map<std::string, std::map<std::string, TestBase*>> TestMapType;
    extern TestMapType* testMap;

    struct FailureInfo {
        const char* message;
        int checkLine;
    };

    class TestBase {
        private:
            static constexpr const char* defaultMessage = "Failed check.";

            const char* suiteName;
            const char* testName;
            const char* fileName;
            int lineNumber;

            void (*setUp) ();
            void (*tearDown) ();

            int checkCount = 0;
            int passCount  = 0;

            std::deque<FailureInfo> failures;

        public:
            TestBase(
                const char* suiteName,
                const char* testName,
                const char* fileName,
                int lineNumber,
                void (*setUp) (),
                void (*tearDown) ()
            ) {
                this->suiteName  = suiteName;
                this->testName   = testName;
                this->fileName   = fileName;
                this->lineNumber = lineNumber;

                this->setUp = setUp;
                this->tearDown = tearDown;

                if (!testMap) testMap = new TestMapType();
                (*testMap)[suiteName][testName] = this;
            }

            bool runTest() {
                if (setUp) setUp();
                testBody();
                if (tearDown) tearDown();
                return checkCount == passCount;
            }

            int getCheckCount() { return checkCount; }
            int getPassCount()  { return passCount; }

            std::string getFailureLog() {
                std::stringstream ret;
                while (!failures.empty()) {
                    FailureInfo info = failures.front();
                    failures.pop_front();

                    ret << "--- FAILURE ---" << std::endl;
                    ret << "Check failed at " << fileName << ":" << info.checkLine << std::endl;
                    ret << "Inside " << suiteName << "." << testName << " (line " << lineNumber << ")." << std::endl;
                    ret << info.message << std::endl;
                }

                return ret.str();
            }

            std::string identify() {
                std::stringstream ret;
                ret << suiteName << "." << testName << " at " << fileName << ":" << lineNumber;
                return ret.str();
            }

        protected:
            bool checkExpression(int lineNumber, bool expr, const char* message = defaultMessage) {
                ++checkCount;
                if (expr) ++passCount;
                if (!expr) {
                    failures.push_back({
                        message,
                        lineNumber,
                    });
                }

                return expr;
            }

        private:
            virtual void testBody() = 0;
    };
}

// This allows for an empty argument list... for some reason...
#define SUZU_TEST_VA_ARGS(...) , ##__VA_ARGS__

/*
 * Checks
 */

#define SUZU_TEST_ASSERT(expr, ...) \
    if (!checkExpression(__LINE__, expr SUZU_TEST_VA_ARGS(__VA_ARGS__))) return

#define SUZU_TEST_EXPECT(expr, ...) \
    checkExpression(__LINE__, expr SUZU_TEST_VA_ARGS(__VA_ARGS__))

/*
 * Suites
 */

#define SUZU_TEST_GET_NAMESPACE_NAME(suite_name) \
    SuzuTest_##suite_name##_Namespace

#define SUZU_TEST_SUITE(suite_name) \
    namespace SUZU_TEST_GET_NAMESPACE_NAME(suite_name) { \
        namespace Suite { \
            typedef void (*WrapperFp)(); \
            const char* name = #suite_name; \
            WrapperFp setUp = 0; \
            WrapperFp tearDown = 0; \
            \
            WrapperFp createAndSetFp(WrapperFp input, WrapperFp& output) { \
                output = input; \
                return input; \
            } \
        } \
    } \
    namespace SUZU_TEST_GET_NAMESPACE_NAME(suite_name)

/*
 * SetUp and TearDown
 */

#define SUZU_TEST_SETUP \
    namespace SetUp { \
        void implementation(); \
        Suite::WrapperFp dummy = Suite::createAndSetFp(&implementation, Suite::setUp); \
    } \
    void SetUp::implementation()

#define SUZU_TEST_TEARDOWN \
    namespace TearDown { \
        void implementation(); \
        Suite::WrapperFp dummy = Suite::createAndSetFp(&implementation, Suite::tearDown); \
    } \
    void TearDown::implementation()

/*
 * Tests
 */

#define SUZU_TEST_GET_CLASS_NAME(test_name) \
    test_name##_Class

#define SUZU_TEST_TEST(test_name) \
    class SUZU_TEST_GET_CLASS_NAME(test_name) : public SuzuTest::TestBase { \
        public: \
            SUZU_TEST_GET_CLASS_NAME(test_name)( \
                const char* testName, \
                const char* fileName, \
                int lineNumber \
            ) : SuzuTest::TestBase(Suite::name, testName, fileName, lineNumber, Suite::setUp, Suite::tearDown) {} \
        private: \
            static SUZU_TEST_GET_CLASS_NAME(test_name)* const instance; \
            void testBody() override; \
    }; \
    \
    SUZU_TEST_GET_CLASS_NAME(test_name)* const SUZU_TEST_GET_CLASS_NAME(test_name)::instance = new SUZU_TEST_GET_CLASS_NAME(test_name)( \
        #test_name, \
        __FILE__, \
        __LINE__ \
    ); \
    \
    void SUZU_TEST_GET_CLASS_NAME(test_name)::testBody()

#ifndef SUZU_TEST_NO_GENERIC_DEFINES
#define ASSERT SUZU_TEST_ASSERT
#define EXPECT SUZU_TEST_EXPECT
#define SUITE SUZU_TEST_SUITE
#define SETUP SUZU_TEST_SETUP
#define TEARDOWN SUZU_TEST_TEARDOWN
#define TEST SUZU_TEST_TEST
#endif

#ifdef SUZU_TEST_DEFINE_MAIN

#include <cstring>
#include <exception>
#include <iomanip>
#include <iostream>

SuzuTest::TestMapType* SuzuTest::testMap = 0;

std::stringstream failureLog;
std::stringstream errorLog;

void listAllTests();
int runSingleTest(char*);
int runSuite(const char*);
int runAllTests();
void cleanTests();
void runTest(SuzuTest::TestBase&);
int summarizeResults();

int testCount  = 0;
int checkCount = 0;
int passCount  = 0;
int errorCount = 0;

int main(int argc, char** argv) {
    if (!SuzuTest::testMap) SuzuTest::testMap = new SuzuTest::TestMapType();
    int ret = 0;
    if (argc >= 2 && strcmp(argv[1], "--list") == 0) {
        listAllTests();
    }
    else if (argc >= 3 && strcmp(argv[1], "--single") == 0) {
        ret = runSingleTest(argv[2]);
    }
    else if (argc >= 3 && strcmp(argv[1], "--suite") == 0) {
        ret = runSuite(argv[2]);
    }
    else {
        ret = runAllTests();
    }
    cleanTests();
    delete SuzuTest::testMap;
    return ret;
}

void listAllTests() {
    for (auto& nameMapP : *SuzuTest::testMap) {
        const char* suiteName = nameMapP.first.c_str();
        auto& nameMap = nameMapP.second;
        for (auto& testP : nameMap) {
            const char* testName = testP.first.c_str();

            std::cout << suiteName << "." << testName << std::endl;
        }
    }
}

int runSingleTest(char* identifier) {
    char* seperator = strchr(identifier, '.');
    if (!seperator) {
        // Invalid format
        return -1;
    }
    *seperator = 0;
    const char* suiteName = identifier;
    const char* testName  = seperator + 1;

    auto nameMapIt = SuzuTest::testMap->find(suiteName);
    if (nameMapIt == SuzuTest::testMap->end()) {
        // Suite not found
        return -1;
    }
    auto& nameMap = nameMapIt->second;

    auto testIt = nameMap.find(testName);
    if (testIt == nameMap.end()) {
        // Test not found
        return -1;
    }

    auto& test = *testIt->second;
    runTest(test);
    return summarizeResults();
}

int runSuite(const char* suiteName) {
    auto nameMapIt = SuzuTest::testMap->find(suiteName);
    if (nameMapIt == SuzuTest::testMap->end()) {
        // Suite not found
        return -1;
    }
    auto& nameMap = nameMapIt->second;
    for (auto& test : nameMap) {
        runTest(*test.second);
    }
    return summarizeResults();
}

int runAllTests() {
    for (auto& nameMap : *SuzuTest::testMap) {
        for (auto& test : nameMap.second) {
            runTest(*test.second);
        }
    }
    return summarizeResults();
}

void cleanTests() {
    for (auto& nameMap : *SuzuTest::testMap) {
        for (auto& test : nameMap.second) {
            delete test.second;
        }
    }
}

void runTest(SuzuTest::TestBase& test) {
    try {
        bool pass = test.runTest();
        std::cout << (pass ? "." : "F");
    }
    catch (std::exception& e) {
        ++errorCount;
        std::cout << "E";

        errorLog << "--- ERROR ---" << std::endl;
        errorLog << "Exception caught in " << test.identify() << std::endl;
        errorLog << e.what() << std::endl;
    }
    catch (...) {
        ++errorCount;
        std::cout << "E";

        errorLog << "--- ERROR ---" << std::endl;
        errorLog << "Unknown error caught in " << test.identify() << std::endl;
    }

    failureLog << test.getFailureLog();

    ++testCount;
    checkCount += test.getCheckCount();
    passCount  += test.getPassCount();
}

int summarizeResults() {
    float passRate = 0.0;
    if (checkCount != 0) passRate = (float)passCount / checkCount * 100;
    std::cout << std::endl << std::endl;
    std::cout << "Tests:  " << testCount << std::endl;
    std::cout << "Checks: " << checkCount << std::endl;
    std::cout << "Errors: " << errorCount << std::endl;
    std::cout << "Passes: " << passCount;
    std::cout << std::setprecision(3) << " (" << passRate << "%)" << std::endl;

    std::cerr << failureLog.str();
    std::cerr << errorLog.str();
    std::cerr << std::flush;

    return checkCount - passCount + errorCount;
}

#endif //ifdef SUZU_TEST_DEFINE_MAIN

#endif //ifndef SUZU_SUZU_TEST_H_DEFINED__
