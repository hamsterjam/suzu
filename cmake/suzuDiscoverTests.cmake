execute_process(
    COMMAND "${TEST_EXECUTABLE}" --list
    OUTPUT_VARIABLE output
)

string(REPLACE "\n" ";" output "${output}")

set(script "")

foreach(line ${output})
    string(APPEND
        script
        "add_test(${line} ${TEST_EXECUTABLE} --single ${line})\n"
    )
endforeach()

file(WRITE "${OUTPUT_FILE}" "${script}")
