function(suzu_discover_tests TARGET)
    set(output "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}_list.cmake")

    add_custom_command(
        TARGET "${TARGET}"
        POST_BUILD
        BYPRODUCTS "${output}"
        COMMAND "${CMAKE_COMMAND}"
            -D "TEST_EXECUTABLE=$<TARGET_FILE:${TARGET}>"
            -D "OUTPUT_FILE=${output}"
            -P "${_SUZU_DISCOVER_TESTS_SCRIPT}"
    )

    set_property(DIRECTORY
        APPEND PROPERTY TEST_INCLUDE_FILES "${output}"
    )
endfunction()

set(_SUZU_DISCOVER_TESTS_SCRIPT "${CMAKE_CURRENT_LIST_DIR}/suzuDiscoverTests.cmake")
