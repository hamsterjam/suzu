Suzu Documentation
==================

* [Overview]
* [Compilation]
* [Testing]
* [Entity Component System]

Overview
--------

This documentation is designed to be descriptive rather than technical, mostly due to the limits of formating in markdown.
This will include code examples but will not, for example, explain every parameter in a systematic way.
I do intend to figure out a better documentation solution at some point, but for now this will suffice.

Compilation
-----------

Suzu is compiled as a library via CMake, compilation is simple

```sh
$ mkdir build && cd build
$ cmake ..
$ make
```

after which the suite of tests can be run via

```sh
$ make test
```

or directly via

```sh
$ ./SuzuTest
```

In the case of compiling for windows via mingw, you must specify the toolchain file as follows

```sh
$ mkdir build && cd build
$ cmake -DCMAKE_TOOLCHAIN_FILE=../mingw-w64-x86_64.cmake ..
$ make
```

Testing
-------

Suzu has a simple testing framework that works similar to [GoogleTest](https://github.com/google/googletest).
The Thief system allows a way to access private members for testing purposes

* [Usage](testing/usage.md)
* [CMake Integration](testing/cmake.md)
* [Thief](testing/thief.md)

Entity Component System
-----------------------

The main state management in use by Suzu is the `EntityTable` which follows an entity component system (ECS) pattern.
This is divided into `Entity`s, `Component`s, `System`s and the `EntityTable`s that tie them together.

* [Overview](ecs/overview.md)
* [Entity](ecs/entity.md)
* [Component](ecs/component.md)
* [System](ecs/system.md)
* [EntityTable](ecs/entitytable.md)

[Overview]: #overview
[Compilation]: #compilation
[Testing]: #testing
[Entity Component System]: #entity-component-system
