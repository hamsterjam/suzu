System
======

[Index](../index.md)

* [Overview]
* [`SystemBase`]
* [`System<E, Ts...>`]
* [`TransformSystem<E, Fn, Ts...>`]
* [`PairTransformSystem<E, Fn, Ts...>`]
* [`ViewSystem<E, Fn, Ts...>`]

Overview
--------
Systems are classes that performs some action over entities in an entity table.
`SystemBase` is provided so an array of systems can be constructed, but systems should typicaly extend `System<E, Ts...>`.

`SystemBase`
------------
A virtual class that is the base of all systems.

### Constructors
```cpp
SystemBase(); // (1)
```

* (1): Default constructor.

### Public Interface
```cpp
virtual void apply() noexcept = 0; // (1)
```

* (1): Applies the system.

`System<E, Ts...>`
------------------
Extends `SystemBase`.

Contains implementations for various helper functions for interacting with an `EntityTable`.
Note that this class is virtual as it does not implement `apply`.

### Template Parameters
* `E`: A class that extends `EntityTable`, typically either `EntityTable` or `BufferedEntityTable`.
* `Ts...`: A list of types used by the components this system expects.

### Constructors
```cpp
System(E& table, Component<Ts>... comps); // (1)
```
* (1): Creates a system over entity table `table` expecting components `comps...`.

### Protected Interface
```cpp
E* table; // (1)
std::tuple<LocalComponent<Ts>...> comps; // (2)
RestrictedTuple<LocalComponent<void>, LocalComponent<Ts>...> nonVoidComps; // (3)

void set<T>(LocalEntity ent, LocalComponent<T> comp, const T& value) noexcept; // (4)
RestrictedTuple<void, Ts...> getAll(LocalEntity ent) const; // (5)
RestrictedTuple<void, Ts...> viewAll(LocalEntity ent) const; // (6)
void setAll(LocalEntity ent, const RestrictedTuple<void, Ts...>& values) noexcept; // (7)
typename E::Iterator getIterator() const noexcept; // (8)
```

* (1): Pointer to the table over which this system acts.
* (2): A tuple containing all components expected by this system.
* (3): A tuple containing all non-void components expected by this system.
* (4): In the case where `E` is `BufferedEntityTable`, calls `BufferedEntityTable::set`. Otherwise calls `EntityTable::get`.
* (5): Returns a tuple containing the values for `ent` of all non-void components expected by this system. Accessed using `EntityTable::get`.
* (6): Returns a tuple containing the values for `ent` of all non-void components expected by this system. Accessed using `EntityTable::view`.
* (7): Sets the values for `ent` of all non-void components expected by this system to `values...` using `System::set`.
* (8): Returns an iterator to entities that possess all components expected by this system.

`TransformSystem<E, Fn, Ts...>`
-------------------------------
Extends `System<E, Ts...>`.

Implements a simple map from current values to new values.
The action is called once for each entity possessing all expected components.
The action should take a tuple containing the current values as a parameter and return a tuple containing the new values.

### Template Parameters
* `E`: A class that extends `EntityTable`, typically either `EntityTable` or `BufferedEntityTable`.
* `Fn`: Any invocable type that takes `std::tuple<Ts...>` as a parameter and returns `std::tuple<Ts...>` excluding any `Ts` that are void.
* `Ts...`: A list of types used by the components this system expects.

### Constructor
```cpp
TransformSystem(E& table, Fn action, Component<Ts>... comps); // (1)
```

* (1): Creates a system over entity table `table` with action `action` expecting components `comps...`.

### Example
```cpp
typedef std::tuple<int> doubleTuple;
doubleTuple doubleAction(doubleTuple in) {
    auto& [value] = in;
    value *= 2;

    return in;
}

Component<int> valueComp;
Component<void> doubleFlag;
table.use(valueComp);
table.use(doubleFlag);

TransformSystem doubleSystem(table, doubleAction, valueComp, doubleFlag);
```

`PairTransformSystem<E, Fn, Ts...>`
-----------------------------------
Extends `System<E, Ts...>`.

Implements a pairwise map from current values to new values.
The action is called once for each pair of entities possessing all expected components.
Do note that this means each entity is used multiple times so care should be taken to not overwrite existing work.
In particular a `PairTransformSystem` is difficult to use meaningfully with a `BufferedEntityTable`.
The action should take tuples containing the current values for each entity and return an array of tuples containing the new values.

### Template Parameters
* `E`: A class that extends `EntityTable`, typically either `EntityTable` or `BufferedEntityTable`.
* `Fn`: Any invocable type that takes `(std::tuple<Ts...>, std::tuple<Ts...>)` as parameters and returns `std::array<std::tuple<Ts...>, 2>` excluding any `Ts` that are void.
* `Ts...`: A list of types used by the components this system expects.

### Constructor
```cpp
TransformSystem(E& table, Fn action, Component<Ts>... comps); // (1)
```

* (1): Creates a system over entity table `table` with action `action` expecting components `comps...`.

### Example
```cpp
typedef std::tuple<int, bool> checkUniqueTuple;
std::array<checkUniqueTuple, 2> checkUniqueAction(checkUniqueTuple in1, checkUniqueTuple in2) {
    auto& [value1, unique1] = in1;
    auto& [value2, unique2] = in2;

    if (value1 == value2) {
        unique1 = false;
        unique2 = false;
    }

    return {in1, in2};
}

Component<int> valueComp;
Component<bool> isUniqueComp;
Component<void> useFlag;
table.use(valueComp);
table.use(isUniqueComp);
table.use(useFlag);

PairTransformSystem checkUniqueSystem(table, checkUniqueAction, valueComp, useFlag, isUniqueComp);
```

`ViewSystem<E, Fn, Ts...>`
--------------------------
Extends `System<E, Ts...>`.

Implements a system that views but does not modify values.
The action is called once for each element that either possesses, or has an ancestor that possesses each of the expected components.
The value passed to the action is either the value of that entity, or the value of the nearest anscestor that possesses the component.
The action should take a tuple contains the current values as a parameter and not return any value.

### Template Parameters
* `E`: A class that extends `EntityTable`, typically either `EntityTable` or `BufferedEntityTable`.
* `Fn`: Any invocable type that takes `std::tuple<Ts...>` as a parameter excluding any `Ts` that are void, and returns `void`.
* `Ts...`: A list of types used by the components this system expects.

### Constructor
```cpp
TransformSystem(E& table, Fn action, Component<Ts>... comps); // (1)
```

* (1): Creates a system over entity table `table` with action `action` expecting components `comps...`.

### Example
```cpp
typedef std::tuple<int, int> printTuple;
void printAction(printTuple in) {
    auto& [value1, value2] = in;
    std::cout << value1 << "," << value2 << std::endl;
}

Component<int> value1Comp;
Component<int> value2Comp;
Component<void> visibleFlag;
table.use(value1Comp);
table.use(value2Comp);
table.use(visibleFlag);

ViewSystem printSystem(table, printAction, visibleFlag, value1Comp, value2Comp);
```

* (1): Creates a system over entity table `table` with action `action` expecting components `comps...`.

[Overview]: #overview
[`SystemBase`]: #systembase
[`System<E, Ts...>`]: #systeme-ts
[`TransformSystem<E, Fn, Ts...>`]: #transformsysteme-fn-ts
[`PairTransformSystem<E, Fn, Ts...>`]: #pairtransformsysteme-fn-ts
[`ViewSystem<E, Fn, Ts...>`]: #viewsysteme-fn-ts
