Entity Component System Overview
================================

[Index](../index.md)

* [Overview]
* [Entities]
* [Components]
* [Systems]

Overview
--------

Entity component system (ECS) state management is at it's core [data-driven programming](https://en.wikipedia.org/wiki/Data-driven_programming).
Game mechanics are written as a sequence of transformations on the data without regard for what that data actually represents.
It should be noted that ,while this is a good way of dealing with a large number of objects with similar properties, it might be suboptimal in other conditions.
I plan to implement a more monolithic state management that works with the ECS similar to Unity's `MonoBehavior`.

An in-game object that has some form of state is known as an *entity*.
For example: enemies, collectables, walls, platforms.

The data state of an entity is divided into *components*, these define properties that the entity possesses.
For example: position, velocity, color, hit points.

The functions that transform the current frames state to the next frames state are known as *systems*.
For example: kinematics, collision detection, damage application.

Unsurprisngly, the entity component system (ECS) state management in Suzu is divided into multiple classes.
An `Entity` represents an ECS entity, a `Component` represents an ECS component, a `System` represents an ECS system.
In addition an `EntityTable` is the container that actually stores and manipulates the data.

Below is a very simplified overview of how these three concepts are use in Suzu.

Entities
--------

An entity is represented by the `Entity` class.
All that needs to be done is declare a variable and specify that an `EntityTable` should use the `Entity`.
Note that multiple `EntityTable`s may use the same `Entity`.
```cpp
Entity myEnt;
table1.use(myEnt);
table2.use(myEnt);
```

Components
----------

A component is represented by the `Component<T>` template class where `T` is the type of the data to be stored.
Similar to `Entity`s, all that must be done is declare a variable and specify that an `EntityTable` should use the `Component`.
By default `Entity`s do not possess `Component`s and ownership and initial values must be declared explicitly.
Special care should be taken with the stored type, if it is not trivially destructible the destructor must be called *manually* via a system.
Note that multiple `EntityTable`s may use the same `Component`.
```cpp
Component<int> myComp;
table1.use(myComp);
table2.use(myComp);

table1.get(ent1, myComp) = 0;
table1.get(ent2, myComp) = 2;
```

Systems
-------

A system is represented by classes that extend the `System` class.
The constructor for the various `System`s take an `EntityTable` to use, a function defining the action the system takes, and any `Component`s the `System` expects `Entities` to posses.
Upon calling `apply()` on the `System` the action will be applied to every `Entity` in the `EntityTable` that possess all of the `Components`.
```cpp
std::tuple<int, int> doubleAction(std::tuple<int, int> in) {
    auto& [comp1, comp2] = in;
    comp1 *= 2;
    comp2 *= 2;

    return in;
}

TransformSystem doubleSystem(table, doubleAction, comp1, comp2);
doubleSystem.apply();
```

[Overview]: #overview
[Entities]: #entities
[Components]: #components
[Systems]: #systems
