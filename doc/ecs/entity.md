Entity
======

[Index](../index.md)

* [Overview]
* [`Entity`]
* [`LocalEntity`]

Overview
--------

There are two classes to represent entities in Suzu, `Entity` and `LocalEntity`.
`Entity` contains a global ID unique to the entity while `LocalEntity` contains an ID that is unique with respect to a specific `EntityTable`.
For the most part an end user should only use `Entity` while leaving `LocalEntity` to internal implementation.

`Entity`
--------

A wrapper for a global ID.

### Constructors
```cpp
Entity(); // (1)
```
* (1): Default constructor

### Example
```cpp
Entity myEnt;
table.use(myEnt);
```

`LocalEntity`
-------------

A wrapper for an ID unique with respect to a specific `EntityTable`.
This should not be constructed manually, it is returned by `EntityTable::local`.
Care should be taken to not use this with other `EntityTable`s as that would give unexpected behaviour.

### Constructors
```cpp
LocalEntity(); // (1)
```
* (1): Default constructor

[Overview]: #overview
[`Entity`]: #entity
[`LocalEntity`]: #localentity
