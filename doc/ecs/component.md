Component
=========

[Index](../index.md)

* [Overview]
* [`Component<T>`]
* [`LocalComponent<T>`]

Overview
--------
There are two classes to represent components in Suzu, `Component<T>` and `LocalComponent<T>`.
`Component<T>` contains a global ID unique to the component while `LocalComponent<T>` contains an ID that is unique with respect to a specific `EntityTable`.
For the most part an end user should only use `Component<T>` while leaving `LocalComponent<T>` to internal implementation.

`Component<T>`
--------------

A wrapper for a global ID.

### Template Parameters
* `T`: The data type used by the component.

### Constructors
```cpp
Component<T>(); // (1)
```
* (1): Default constructor

### Example
```cpp
Component<int> myComp;
table.use(myComp);
```

`LocalComponent<T>`
-------------------
A wrapper for an ID unique with respect to a specific `EntityTable`.
This should not be constructed manually, it is returned by `EntityTable::local`.
Care should be taken to not use this with other `EntityTable`s as that would give unexpected behaviour.

### Template Parameters
* `T`: The data type used by the component.

### Constructors
```cpp
LocalComponent<T>(); // (1)
```
* (1): Default constructor

[Overview]: #overview
[`Component<T>`]: #componentt
[`LocalComponent<T>`]: #localcomponentt
