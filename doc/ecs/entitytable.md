Entity Table
============

[Index](../index.md)

* [Overview]
* [`EntityTable`]
* [`EntityTable::Iterator`]
* [`BufferedEntityTable`]

Overview
--------
Entity Tables are the class where the data is actually stored for the entity component system state management.
The `EntityTable` class is a simple data table, where as `BufferedEntityTable` contains a second buffer so the current state can be maintained while modifying the next state.

`EntityTable`
-------------
Stores the data for Suzus entity component system state management.

### Constructors
```cpp
EntityTable() noexcept; // (1)
EntityTable(EntityTable&& other) noexcept; // (2)
```

* (1): Default constructor.
* (2): Move constructor.

### Public Interface
```cpp
void use<T>(Component<T> comp) noexcept; // (1)
void use(Entity ent) noexcept; // (2)

LocalComponent<T> local<T>(Component<T> comp) const; // (3)
LocalEntity local(Entity ent) const; // (4)

T& get<T>(LocalEntity ent, LocalComponent<T> comp) noexcept; // (5)
T& get<T>(Entity ent, LocalComponent<T> comp); // (6)
T& get<T>(LocalEntity ent, Component<T> comp); // (7)
T& get<T>(Entity ent, Component<T> comp); // (8)

const T& cGet<T>(LocalEntity ent, LocalComponent<T> comp) const; // (9)
const T& cGet<T>(Entity ent, LocalComponent<T> comp) const; // (10)
const T& cGet<T>(LocalEntity ent, Component<T> comp) const; // (11)
const T& cGet<T>(Entity ent, Component<T> comp) const; // (12)

const T& view<T>(LocalEntity ent, LocalComponent<T> comp) noexcept; // (13)
const T& view<T>(Entity ent, LocalComponent<T> comp); // (14)
const T& view<T>(LocalEntity ent, Component<T> comp); // (15)
const T& view<T>(Entity ent, Component<T> comp); // (16)

const T& cView<T>(LocalEntity ent, LocalComponent<T> comp) const; // (17)
const T& cView<T>(Entity ent, LocalComponent<T> comp) const; // (18)
const T& cView<T>(LocalEntity ent, Component<T> comp) const; // (19)
const T& cView<T>(Entity ent, Component<T> comp) const; // (20)

bool& owns(LocalEntity ent, LocalComponent<void> comp) noexcept; // (21)
bool& owns(Entity ent, LocalComponent<void> comp); // (22)
bool& owns(LocalEntity ent, Component<void> comp); // (23)
bool& owns(Entity ent, Component<void> comp); // (24)

bool cOwns(LocalEntity ent, LocalComponent<void> comp) const noexcept; // (21)
bool cOwns(Entity ent, LocalComponent<void> comp) const; // (22)
bool cOwns(LocalEntity ent, Component<void> comp) const; // (23)
bool cOwns(Entity ent, Component<void> comp) const; // (24)

bool has(LocalEntity ent, LocalComponent<void> comp) const noexcept; // (29)
bool has(Entity ent, LocalComponent<void> comp) const; // (30)
bool has(LocalEntity ent, Component<void> comp) const; // (31)
bool has(Entity ent, Component<void> comp) const; // (32)

void adopt(LocalEntity child, LocalEntity comp) noexcept; // (33)
void adopt(Entity child, LocalEntity comp); // (34)
void adopt(LocalEntity child, Entity comp); // (35)
void adopt(Entity child, Entity comp); // (36)

bool& orphan(LocalEntity ent) noexcept; // (37)
bool& orphan(Entity ent); // (38)

EntityTable::Iterator begin<Ts...>(Ts... args) const; // (39)
EntityTable::Iterator end() const noexcept; // (40)
```

* (1): Instructs the table to use `comp` and allocates enough data to store its value for each entity.
* (2): Instructs the table to use `ent` and allocates enough data to store a value for each component.
* (3): Fetch the `LocalComponent<T>` corresponding to `comp` for this table.
* (4): Fetch the `LocalEntity` corresponding to `ent` for this table.
* (5~8): Returns a reference to the value of `comp` corresponding to `ent`.
This function implies `ent` possesses `comp`.
Cannot be called when `T` is void.
* (9~12): Get a const reference to the value of `comp` corresponding to `ent` (see `get`).
Does *not* imply that `ent` possesses `comp` and will throw instead.
Cannot be called when `T` is void.
* (17~20): Returns the value of comp corresponding to `ent` or the nearest ancestor of `ent` that possesses `comp`.
If no such ancestor exists, returns the value corresponding to the furthest ancestor (implying possession).
Cannot be called when `T` is void.
* (21~24): Returns the value of comp corresponding to `ent` or the nearest ancestor of `ent` that possesses `comp` (see `view`).
If no such ancestor exists this does *not* imply possession by the furthest ancestor and will throw instead.
Cannot be called when `T` is void.
* (21~24): Returns whether `ent` possesses `comp`. Returns as a reference so you can set the value using this function.
* (25~28): Returns whether `ent` possesses `comp`.
* (29~32): Returns whether `ent`, or any ancestor of `ent` possess `comp`.
* (33~36): Sets `parent` to be the parent of `child`
* (37~38): Returns whether `ent` is an orphan, that is has no parent. Returns a reference so you can set the value using this function.
* (39): Returns an iterator that iterates over all entities that possess all of `args...`.
All `Ts` must be either `Component<void>` or `LocalComponent<void>`.
* (40): Returns the past-the-end iterator.

### Example
```cpp
EntityTable table;

Component<int> comp;
Component<void> flag;
table.use(comp);
table.use(flag);

Entity ent;
Entity parent;
table.use(ent);
table.use(parent);

table.get(ent, comp) = 1;

table.owns(ent, comp); // true
table.get(ent, comp);  // 1

// Parenting
table.adopt(ent, parent);
table.get(parent, comp) = 2;

table.orphan(ent);     // false
table.has(ent, comp);  // true
table.view(ent, comp); // 2

table.get(ent, comp) = 1;

table.has(ent, comp);  // true
table.view(ent, comp); // 1

table.owns(ent, comp) = false;
table.orphan(ent) = true;

table.has(ent, comp); // false
```

`EntityTable::Iterator`
-----------------------
An object to assist with iterating through entities.
It maintains a list of required components and only returns entities that possess all of them.
This should not be constructed directly but instead obtained from `EntityTable::begin`.

### Public Interface
```cpp
void require(LocalComponent<void> comp) noexcept; // (1)
Iterator view(bool view = true) noexcept; // (2)

Iterator& operator++() noexcept; // (3)
LocalEntity operator*() noexcept; // (4)
```

* (1): Add `comp` to the list of required components.
* (2): Returns the view iterator, this checks components with `EntityTable::has` instead of `EntityTable::owns`.
* (3): Increment the iterator to the next valid entity.
* (4): Retrieve the current entity.

### Example
```cpp
EntityTable table;

Entity ents[10];

Component<int> comp;
Component<void> flag;

table.use(comp);
table.use(flag);

for (int i = 0; i < 10; ++i) {
    table.use(ents[i]);
    table.get(ents[i], comp) = i;
    table.owns(ents[i], flag) = (i % 2 == 0);
}

for (auto it = table.begin(comp, flag); it != table.end(); ++it) {
    table.get(*it, comp); // 0, 2, 4, 6, 8
}
```

`BufferedEntityTable`
---------------------
Extends `EntityTable`.

Similar to `EntityTable` but stores a second table that can be accessed separately.
This allows the current state to be maintained while modifying the next state.

### Constructors
```cpp
BufferedEntityTable(); // (1)
BufferedEntityTable(BufferedEntityTable&& other); // (2)
```

* (1): Default constructor.
* (2): Move constructor.

### Public Interface
```cpp
void set<T>(LocalEntity ent, LocalComponent<T> comp, T value) noexcept; // (1)
void set<T>(Entity ent, LocalComponent<T> comp, T value); // (2)
void set<T>(LocalEntity ent, Component<T> comp, T value); // (3)
void set<T>(Entity ent, Component<T> comp, T value); // (4)

void swap() noexcept; // (5)
```

* (1~4): Set the value of `comp` corresponding to `ent` to `value` in the non-active buffer.
* (5): Swap the non-active and active buffers

### Example
```cpp
Entity ent;
Component<int> comp;
BufferedEntityTable table;

table.use(ent);
table.use(comp);

table.get(ent, comp) = 1;
table.set(ent, comp, 2);

table.get(ent, comp); // 1

table.swap();

table.get(ent, comp); // 2
```

[Overview]: #overview
[`EntityTable`]: #entitytable
[`EntityTable::Iterator`]: #entitytableiterator
[`BufferedEntityTable`]: #bufferedentitytable
