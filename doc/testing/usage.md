Testing Usage
=============

[Index](../index.md)

* [Overview]
* [Suites]
* [Tests]
* [SetUp and TearDown]
* [Checks]
* [Non-Generic Macros]
* [Main File]
* [Running Tests]

Overview
--------

The testing component of Suzu is a simple, single file, header only library that provides macros that make testing simpler.
To access the relevant macros simply include the `SuzuTest.hpp` in your test files.

Suites
------

Test suites are declared with the the `SUITE` macro.
The suite name must be a valid, globaly unique C++ identifier.
The macro functions as a normal C++ `namespace` declaration.

```cpp
SUITE(MyTestSuite) {
    // ...
}
```

Tests
-----

Tests are declared within a suit with the `TEST` macro.
A test must be declared within a suite and the test name must be a valid C++ identifier that is unique within the suite.
The macro functions as a normal C++ function declaration with no return value.

```cpp
SUITE(MyTestSuite) {
    TEST(MyTest) {
        // ...
    }
}
```

SetUp and TearDown
------------------

The `SETUP` and `TEARDOWN` macros declare functions that are called before and after (respectively) each test in a suite.
They must be declared within a suite and each can only appear once within each suite.
The macros function as normal C++ function declarations with no return values.

```cpp
SUITE(MyTestSuite) {
    Foo* foo;
    SETUP {
        foo = new Foo();
    }

    TEARDOWN {
        delete foo;
    }

    // ...
}
```

Checks
------

Checks are performed with the `ASSERT` and `EXPECT` macros.
A test will end execution upon a failed `ASSERT` but continue after a failed `EXPECT` but are otherwise identical.
They must be called from within a test and take two parameters, an expression to test and an optional error message.

```cpp
SUITE(MyTestSuite) {
    Test(MyTest) {
        int x = 2
        ASSERT(x > 0, "x is positive");         // Pass, test continues
        EXPECT(x == 2);                         // Pass, test continues
        EXPECT(x > 10, "x is greater than 10"); // Fail, test continues
        ASSERT(x != 2);                         // Fail, test ends

        ASSERT(true);  // Not run
    }
}
```

Non-Generic Macros
------------------

Non-generic versions of all test macros are provided incase of name collisions.
They are summarised in the following table:

| Generic Macro | Non-Generic Macro    |
|---------------|----------------------|
| `SUITE`       | `SUZU_TEST_SUITE`    |
| `TEST`        | `SUZU_TEST_TEST`     |
| `SETUP`       | `SUZU_TEST_SETUP`    |
| `TEARDOWN`    | `SUZU_TEST_TEARDOWN` |
| `ASSERT`      | `SUZU_TEST_ASSERT`   |
| `EXPECT`      | `SUZU_TEST_EXPECT`   |

By defining `SUZU_TEST_NO_GENERIC_DEFINES` before including SuzuTest.hpp generic macros will not be defined

Main File
---------

Exactly one file must define `SUZU_TEST_DEFINE_MAIN` *before* including `SuzuTest.hpp`.
This defines a `main` function that runs the tests.
In the tests for Suzu, this is simply a file containing the following:

```cpp
#define SUZU_TEST_DEFINE_MAIN
#include "SuzuTest.hpp"
```

Running Tests
-------------

All tests are compiled together to an executable used to run the tests, in the case of Suzu this is `SuzuTest`.
Calling the program will run all tests

```sh
$ ./SuzuTest
```
A specific suite can be run using the `--suite` parameter

```sh
$ ./SuzuTest --suite MyTestSuite
```

A specific test can be run using the `--single` parameter.
The format for the name is `[suite name].[test name]`.

```sh
$ ./SuzuTest --single MyTestSuite.MyTest
```

Finally, a list of all tests can be obtained with the `--list` parameter.
The format for each name is `[suite name].[test name]`.

```sh
$ ./SuzuTest --list
```

[Overview]: #overview
[Suites]: #suites
[Tests]: #tests
[SetUp and TearDown]: #setup-and-teardown
[Checks]: #checks
[Non-Generic Macros]: #non-generic-macros
[Main File]: #main-file
[Running Tests]: #running-tests
