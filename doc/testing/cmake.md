Testing CMake Integration
=========================

[Index](../index.md)

The executable created by SuzuTest can be queried with the `--list` parameter to output all available tests.
By setting a post-build command, we can automatically populate the list of tests.
To accomplish this, simply include `suzu.cmake` in your CMakeLists file and run `suzu_discover_tests`.
The Suzu CMakeLists file uses the following:

```cmake
include("${CMAKE_CURRENT_LIST_DIR}/cmake/suzu.cmake")
enable_testing()
suzu_discover_tests(SuzuTest)
```
