Thief
=====

[Index](../index.md)

* [Overview]
* [Usage]
* [`MAKE_THIEF`]
* [`STEAL`]
* [Example]

Overview
--------

Provides a simple way to access private members for testing purposes.
Obviously this should not ever be used outside of tests.

This is adapted from [this stack overflow answer](https://stackoverflow.com/a/3173080).

Usage
-----

The `Thief.hpp` header provides two macros to assist with accessing private members.
The `MAKE_THIEF` macro uses "friend injection" to create a friend function that accesses a private member.
The `STEAL` function uses the generated function to access the variable.

The macros `SUZU_THIEF_MAKE_THIEF` and `SUZU_THIEF_STEAL` behave identically to `MAKE_THIEF` and `STEAL`.
These are provided in case the generic names would cause a name conflict.
In this case, by defining `SUZU_THIEF_NO_GENERIC_DEFINES` before including Thief.hpp `MAKE_THIEF` and `STEAL` will not be defined.

`MAKE_THIEF`
------------

The `MAKE_THIEF` macro takes either 3 or 4 arguments as detailed below
```cpp
MAKE_THIEF(targetClass, memberType, memberName); // (1)
MAKE_THIEF(identifier, targetClass, memberType, memberName); // (2)
```
This macro creates a function that `STEAL` uses to access `memberType targetClass::memberName` regardless of visibility.
In the case of (1), `targetClass` acts as an identifier for this function and must be a valid C++ identifier.
In the case where it is not a valid C++ identifier (e.g. template classes) a seperate identifier can be provided using (2).

`STEAL`
-------
The `STEAL` macro takes 3 arguments as detailed below
```cpp
STEAL(variable, identifier, memberName); // (1)
```
This macro uses the function created by `MAKE_THIEF` to access `memberName` in `variable` regardless of visibility.
The macro returns by reference so the target member can be either set or viewed.
`identifier` is the identifier provided to `MAKE_THIEF`.

Example
-------

```cpp
#include <cassert>
#include "Thief.hpp"

class Foo {
    private:
        int value;

    public:
        Foo(int value) { this->value = value; }
        int get() { return value; }
};

MAKE_THIEF(Foo, int, value);

int main(int argc, char** argv) {
    Foo foo(2);
    assert(STEAL(foo, Foo, value) == 2);

    STEAL(foo, Foo, value) = 3;
    assert(foo.get() == 3);

    return 0;
}
```

[Overview]: #overview
[Usage]: #usage
[`MAKE_THIEF`]: #make_thief
[`STEAL`]: #steal
[Example]: #example
